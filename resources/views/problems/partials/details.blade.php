<div class="card">
	<div class="card-body">
		<h3>{{ $problem->name }}</h3>
		<p>{{ $problem->description }}</p>
	</div>
</div>

<div class="card">
	<div class="card-body">
		<h3>Restrições</h3>
		<p><strong>Número de Cromossomos: </strong>{{ $problem->chromosome_quantity or 'Sem Restrição' }}</p>
		<p><strong>Intervalo de valores dos cromossomos: </strong>{{ $problem->min_val or 'Sem Restrição' }} - {{ $problem->max_val or 'Sem Restrição' }}</p>
	</div>
</div>