<label for="problem_id">Problemas</label>
<select name="problem_id" class="form-control" id="problem_select" required>
	<option value="">Selecione</option>
	@foreach ($problems as $problem)
		<option value="{{ $problem->id }}">{{ $problem->name }}</option>
	@endforeach
</select>