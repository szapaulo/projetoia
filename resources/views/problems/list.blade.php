<table class="table table-striped table-dark">
	<thead>
		<tr class="bg-success text-dark">
			<th>Título</th>
			<th>Descrição</th>
		</tr>
	</thead>
	<tbody>

	@foreach ($problems as $problem)
	<tr>
		<td><a class="" href="{{ route('problemas.show', $problem->id) }}">{{ $problem->name }}</a></td>
		<td>{{ $problem->description }}</td>
	</tr>
		
	@endforeach

	</tbody>
</table>