@extends('layouts.app')
@php
	$subtitle = 'Exibir';
	$title = "Problemas";
@endphp
@section('content')

@include('problems.show')
<form method="POST" action="{{ route('solucao.criar') }}" accept-charset="UTF-8">
		{{ csrf_field() }}
		<input type="hidden" name="problem_id" value="{{ $problem->id }}">
  		<button type="submit" class="btn btn-outline-dark">Propor solução</button>
</form>
<h3>Soluções</h3>
@include('solutions.list2')

@endsection