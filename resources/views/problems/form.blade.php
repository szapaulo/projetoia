<div class="form-group">
  <label for="name">Título</label>
  <input type="text" class="form-control" id="title" name="name">
</div>
<div class="form-group">
  <label for="description">Descrição</label>
  <textarea class="form-control" id="description" name="description"></textarea>
</div>
<button type="submit" class="btn btn-primary">Salvar</button>
