@extends('layouts.app')
@php
	$subtitle = 'Criar';
	$title = "Problemas";
@endphp
@section('content')
	<form method="POST" action="{{ route('problemas.store') }}" accept-charset="UTF-8">
		{{ csrf_field() }}
		@include('problems.form')
	</form>
@endsection