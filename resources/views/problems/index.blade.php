@extends('layouts.app')
@php
	$subtitle = 'Listar';
	$title = "Problemas";
@endphp
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="text-center">
			<a href="{{ route('new_ag') }}" class="btn btn-outline-success btn-lg">Adicionar</a>
		</div>
		<br>
	</div>
	<div class="col-md-12">
		@include('problems.list')
	</div>
</div>



@endsection