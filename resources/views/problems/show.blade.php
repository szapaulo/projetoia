<table class="table table-striped table-bordered">
	<thead>
		<tr class="table-info">
			<th>Título</th>
			<th>Descrição</th>
		</tr>
	</thead>
	<tbody>

	<tr>
		<td>{{ $problem->name }}</td>
		<td>{{ $problem->description }}</td>
	</tr>
	</tbody>
</table>