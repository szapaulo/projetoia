<table class="table">
	<thead>
		<tr>
			<th colspan="4" class="text-center">
				<strong>GERAÇÃO {{ $population->generation }}</strong>
			</th>
		</tr>
		<tr>
			<th>Rank</th>
			<th>Individuo</th>
			<th>Cromossomos</th>
			<th>Resultado</th>
		</tr>
	</thead>
	<tbody>
		@php
			$rank = 1;
			$selected = explode(',', $population->selected);
		@endphp
		@foreach ($population->individuals()->orderBy('pivot_rank', 'desc')->get() as $key => $individual)
		<tr class="{{in_array($individual->id, $selected)? 'alert alert-success': 'alert alert-default'}}">
			<td>{{$rank}}º</td>
			<td>Individuo {{$individual->pivot->index}}</td>
			<td>
				<span class="individual" data-iid="{{ $individual->id }}" data-parent1="{{ $individual->parent1_id }}" data-parent2="{{ $individual->parent2_id }}">
				@foreach ($individual->intChromosomes as $chromosome)
					<span class="gene {{ $chromosome->mutated? 'mutated': '' }}" data-gid="{{ $chromosome->id }}" data-parent="{{ $chromosome->parent_id }}">{{$chromosome->value}}</span>
				@endforeach
				</span>
			</td>
			<td>{{$individual->fitness_solution}}</td>
		</tr>
		@php
			$rank++;
		@endphp
		@endforeach

	</tbody>
</table>