<table class="table">
	<thead>
		<tr>
			<th>Individuo</th>
			<th>Cromossomos</th>
			<th>Resultado</th>
		</tr>
	</thead>
	<tbody>
		
		@foreach ($population->individuals as $key => $individual)
		<tr>
			<td>Individuo {{$key+1}}</td>
			<td>
				@foreach ($individual->intChromosomes as $chromosome)
					<span class="gene">{{$chromosome->value}}</span>
				@endforeach
			</td>
			<td>{{ $individual->fitness_solution }}</td>
		</tr>
		@endforeach

	</tbody>
</table>