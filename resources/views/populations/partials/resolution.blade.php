@foreach ($resolution->populations as $population)
	<div class="col-md-6">
		@include('populations.partials.selected', ['population' => $population])
	</div>
@endforeach