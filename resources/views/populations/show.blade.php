{{$pop_number = "0"}}
@foreach ($population->individuals as $ind_number => $individual)
<div class="individual-content individual" data-id="{{ $individual->id }}" data-parent1="{{ $individual->parent1_id }}" data-parent2="{{ $individual->parent2_id }}">
  

  <a class="card-link" data-toggle="collapse" href="#individual{{$pop_number}}{{$ind_number}}">
    Individuo {{ $ind_number+1 }}
  </a>
  <span class="chromosome-content">
    
    @foreach ($individual->intChromosomes as $chromo_number => $chromossome)
      <span class="chromosome border border-dark" data-id="{{ $chromossome->id }}" data-parent="{{ $chromossome->parent_id }}">{{ $chromossome->value }} </span>
    @endforeach
  </span>
</div>
@endforeach