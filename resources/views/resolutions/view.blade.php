@extends('layouts.app')
@php
	$subtitle = 'Exibir';
	$title = "Resolução";
@endphp
@section('content')




 <div id="accordion">

 	 @foreach ($resolution->populations as $pop_number => $population)
 	 <div class="card">
    <div class="card-header">
      <a class="card-link" data-toggle="collapse" href="#population{{$pop_number}}">
        População {{ $pop_number }}
      </a>
    </div>
    <div id="population{{$pop_number}}" class="collaps">
      <div class="card-body">



@foreach ($population->individuals()->orderBy('fitness_solution')->get() as $ind_number => $individual)
<div class="individual-content individual" data-id="{{ $individual->id }}" data-parent1="{{ $individual->parent1_id }}" data-parent2="{{ $individual->parent2_id }}">
  

  <a class="card-link" data-toggle="collapse" href="#individual{{$pop_number}}{{$ind_number}}">
    Individuo {{ $ind_number+1 }} - {{ $individual->fitness_solution }}
  </a>
  <span class="chromosome-content">
    
    @foreach ($individual->intChromosomes as $chromo_number => $chromossome)
      <span class="chromosome border border-dark" data-id="{{ $chromossome->id }}" data-parent="{{ $chromossome->parent_id }}">{{ $chromossome->value }} </span>
    @endforeach
  </span>
</div>
@endforeach



      </div>
    </div>
  </div>

 @endforeach
</div>

  

@endsection