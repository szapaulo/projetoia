@extends('layouts.app')
@php
	$subtitle = 'Exibir';
	$title = "Problema";
@endphp
@section('content')

@include('solutions.show')

<form method="POST" action="{{ route('resolucao.rodar') }}" accept-charset="UTF-8">
	{{ csrf_field() }}
	<input type="hidden" name="solution_id" value="{{ $solution->id }}">
	<button type="submit" class="btn btn-outline-dark">Rodar solução</button>
</form>	



@endsection