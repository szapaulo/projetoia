<table>
	<thead>
		<tr class="table-info">
			<th>Título</th>
			<th>Exibir</th>
		</tr>
	</thead>
	<tbody>
	@foreach ($solutions as $solution)
	<tr>
		<td>{{ $solution->name }}</td>
		<td><a href="{{ route('solucoes.show', $solution->id) }}">Ver</a></td>
	</tr>		
	@endforeach
	</tbody>
</table>