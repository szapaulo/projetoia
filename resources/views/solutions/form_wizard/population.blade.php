<div class="tab">População:
	<div class="form-row">
		<div class="form-group col-md-4">
			<label for="type">Tipo de dado do chromossomo</label>
			<select name="type" class="form-control custom-select">
				<option selected>Selecione</option>
				<option value="1">Inteiro</option>
				<option value="2">Binário</option>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="population_size">Tamanho da população</label>
			<input type="number" name="population_size" class="form-control" placeholder="Quantidade de indivíduos">
		</div>

		<div class="form-group col-md-4">
			<label for="individual_size">Tamanho do inidividuo</label>
			<input type="number" name="individual_size" id="individual_size" class="form-control" placeholder="Quantidade de cromossomos">
		</div>
	</div>
</div>