<div class="tab">Avaliação:
	<div class="form-row">
		<div class="form-group col-md-6">
			<label for="formula">Função Fitness</label>
			<input type="text" name="formula" id="formula" class="form-control" hidden>
			<input type="text" name="formula-display" id="formula-display" class="form-control" disabled>
		</div>
	</div>
	
				<div id="solution_options">
					<div class="row">
						<div id="solution_chromosomes" class="btn-group" role="group" aria-label="Basic example">
							
						</div>						
					</div>
					<div id="solution_operations" class="row">
						<div class="btn-group" role="group" aria-label="Basic example">
							<button type="button" class="btn btn-dark operator" data="+">+</button>
							<button type="button" class="btn btn-dark operator" data="-">-</button>
							<button type="button" class="btn btn-dark operator" data="*">*</button>
							<button type="button" class="btn btn-dark operator" data="/">/</button>
							<button type="button" class="btn btn-dark operator" data="(">(</button>
							<button type="button" class="btn btn-dark operator" data=")">)</button>
						</div>
					</div>
					<div class="row">
						<div class="btn-group" role="group" aria-label="Basic example">
							<button type="button" class="btn btn-dark operator" data="0">0</button>
							<button type="button" class="btn btn-dark operator" data="1">1</button>
							<button type="button" class="btn btn-dark operator" data="2">2</button>
							<button type="button" class="btn btn-dark operator" data="3">3</button>
							<button type="button" class="btn btn-dark operator" data="4">4</button>
							<button type="button" class="btn btn-dark operator" data="5">5</button>
							<button type="button" class="btn btn-dark operator" data="6">6</button>
							<button type="button" class="btn btn-dark operator" data="7">7</button>
							<button type="button" class="btn btn-dark operator" data="8">8</button>
							<button type="button" class="btn btn-dark operator" data="9">9</button>
							
						</div>						
					</div>
				</div>
</div>