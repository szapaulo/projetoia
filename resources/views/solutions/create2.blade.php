@extends('layouts.app')
@php
	$subtitle = '';
	$title = "Solução";
@endphp
@section('content')
<div class="row">
	<div class="col-md-4">
		<h3>Problema</h3>
		@include('problems.show')
	</div>
	<div class="col-md-8">
	<div class="">
		<h3>Propor Solução</h3>
		<form method="POST" action="{{ route('solucao.calcular') }}" accept-charset="UTF-8">
		{{ csrf_field() }}
		@include('solutions.smartwizard')
	</form>
	</div>
	</div>
	
</div>
	
	
@endsection