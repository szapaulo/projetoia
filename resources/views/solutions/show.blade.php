@extends('layouts.app')
@php
	$subtitle = 'Exibir';
	$title = "Problema";
@endphp
@section('content')
<table class="table table-striped">
	<thead class="thead-dark">
		<tr>
			<th>Título</th>
			<th>Descrição</th>
		</tr>
	</thead>
	<tbody>

	<tr>
		<td>{{ $solution->name }}</td>
		<td>{{ $solution->description }}</td>
	</tr>
	</tbody>
</table>
<br>
<form method="POST" action="{{ route('resolucao.rodar') }}" accept-charset="UTF-8">
		{{ csrf_field() }}
		<input type="hidden" name="solution_id" value="{{ $solution->id }}">
  		<button type="submit" class="btn btn-outline-dark">Rodar solução</button>
	</form>	
	<br>


@endsection