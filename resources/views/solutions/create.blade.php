@extends('layouts.app')
@php
	$subtitle = '';
	$title = "Solução";
@endphp
@section('content')
<div class="row">
	<div class="col-md-4">
	<div class="">
		<h3>Problema</h3>
		<table class="table">
		<thead class="thead-light">
			<th>{{ $problem->name }}</th>
		</thead>
		<tbody>
		<tr>
			<td>
				{{ $problem->description }}				
			</td>
		</tr>
		</tbody>
	</table>
	</div>
	</div>
	<div class="col-md-8">
	<div class="">
		<h3>Propor Solução</h3>
		<form method="POST" action="{{ route('solucao.calcular') }}" accept-charset="UTF-8">
		{{ csrf_field() }}
		@include('solutions.form2')
	</form>
	</div>
	</div>
	
</div>
	
	
@endsection