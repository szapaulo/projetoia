<input type="hidden" name="problem_id" value="{{ $problem->id }}">

  <div class="form-row">
<div class="form-group col-md-4">
	<label for="name">Título</label>
	<input type="text" class="form-control" id="name" name="name">
</div>  
<div class="form-group col-md-8">
	<label for="description">Descrição</label>
	<textarea class="form-control" id="description" name="description"></textarea>
</div>
</div>

<div class="row">
	<div class="col-md-6">
		<fieldset class="form-group">
	<legend>Configuração da População</legend>
	<label for="population_size">Tamanho</label>
	<input type="number" name="population_size" class="form-control" placeholder="Quantidade de indivíduos">
</fieldset>

	</div>
	<div class="col-md-6">
		<fieldset class="form-group">
	<legend>Configuração do Individuo</legend>
	<label for="individual_size">Tamanho</label>
	<input type="number" name="individual_size" class="form-control" placeholder="Quantidade de cromossomos">
</fieldset>
	</div>
	

</div>


<fieldset class="form-group">
	<legend>Configuração do Cromossomo</legend>
	<div class="form-row">
		<div class="form-group col-md-3">
	<label for="type">Tipo</label>
		<select name="type" class="form-control custom-select">
			<option selected>Selecione</option>
			<option value="1">Inteiro</option>
			<option value="2">Binário</option>
		</select>
	</div>
	<div class="form-grou col-md-3">
		<label for="chromosome_size">Tamanho</label>
		<input type="number" name="chromosome_size" class="form-control">
	</div>
	<div class="form-group col-md-3">
		<label for="chromosome_initial_value">Valor Mínimo</label>
		<input type="number" name="chromosome_initial_value" class="form-control">
	</div>
	<div class="form-group col-md-3">
		<label for="chromosome_final_size">Valor Máximo</label>
		<input type="number" name="chromosome_final_size" class="form-control">
	</div>
	</div>
	
</fieldset>
<fieldset class="form-group">
	<legend>Solução</legend>
  <div class="form-row">
  <div class="form-group col-md-6">
  	<label for="formula">Função Fitness</label>
  	<input type="text" name="formula" class="form-control">
  </div>
  <div class="form-group col-md-6">
  	<label for="optimal_solution">Solução Ótima</label>
  	<input type="text" name="optimal_solution" class="form-control">
  </div>
  </div>
</fieldset>
<div class="text-right">
  <button type="submit" class="btn btn-primary pull-right">Rodar</button>
</div>
<br>