@extends('layouts.app')
@php
	$subtitle = '';
	$title = "Solução";
@endphp
@section('content')
<div class="row">
<!--
    <div class="col-md-2">
        <h4>Problema</h4>
        <table class="table">
            <thead>
                <tr>
                    <th>One Max</th>
                </tr>
                <tr>
                    <td>
                        Maximizar valores verdare (bit 1)
                    </td>
                </tr>
            </thead>
        </table>
    </div> -->

    <div class="col-md-12">
        {{ csrf_field() }}
        <input type="hidden" id="problem_id" name="problem_id" value="1">
        <input type="hidden" name="solution_id" id="solution_id" value="">
        <div id="smartwizard">
            <ul>
            	@include('solutions.smart_tabs.step1.title')
            	@include('solutions.smart_tabs.step2.title')
            	@include('solutions.smart_tabs.step3.title')
            	@include('solutions.smart_tabs.step4.title')
            	@include('solutions.smart_tabs.step5.title')
            	{{-- @include('solutions.smart_tabs.step6.title') --}}
            </ul>

            <div>
            	@include('solutions.smart_tabs.step1.body')
            	@include('solutions.smart_tabs.step2.body')
            	@include('solutions.smart_tabs.step3.body')
            	@include('solutions.smart_tabs.step4.body')
            	@include('solutions.smart_tabs.step5.body')
            	{{-- @include('solutions.smart_tabs.step6.body') --}}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection