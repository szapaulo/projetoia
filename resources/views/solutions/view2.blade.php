@extends('layouts.app')
@php
	$subtitle = 'Solução';
	$title = "Soluções";
@endphp
@section('content')
<h3>Solução</h3>
<br>
<table  class="table">
	
@foreach ($populations as $key => $population)
<thead class="thead-dark">
	<tr>
		<th colspan="3">População {{ $key }}</th>
	</tr>
</thead>
<thead class="thead-light">		
		<tr>
			<th>geração</th>
			<th>desempenho</th>
			<th>cromossomos</th>
		</tr>
	</thead>
@foreach ($population as $individual)
	<tr>
		<td>{{ $individual['generation'] }}</td>
		<td>{{ $individual['rating'] }}</td>
		<td>|
		@foreach ($individual['chromossomes'] as $chromossome)
			{{ $chromossome }} |
		@endforeach

		</td>
	</tr>
		
	@endforeach
	<tr></tr>
	@endforeach
</table>
	
@endsection