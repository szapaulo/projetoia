@extends('layouts.app')
@php
	$subtitle = '';
	$title = "Solução";
@endphp
@section('content')
<div class="row">
	<div class="col-md-12">
		<form id="regForm" class="center-block" action="">

			<h1 class="center-block">Criar Solução</h1>
			
			@include('solutions.form_wizard.population')
			@include('solutions.form_wizard.chromosome')
			@include('solutions.form_wizard.initial_population')
			@include('solutions.form_wizard.new_populations')
			@include('solutions.form_wizard.avaliation')
			@include('solutions.form_wizard.stop_condition')
			<!-- One "tab" for each step in the form: 
			<div class="tab">Name:
			  <p><input placeholder="First name..." oninput="this.className = ''"></p>
			  <p><input placeholder="Last name..." oninput="this.className = ''"></p>
			</div>

			<div class="tab">Contact Info:
			  <p><input placeholder="E-mail..." oninput="this.className = ''"></p>
			  <p><input placeholder="Phone..." oninput="this.className = ''"></p>
			</div>

			<div class="tab">Birthday:
			  <p><input placeholder="dd" oninput="this.className = ''"></p>
			  <p><input placeholder="mm" oninput="this.className = ''"></p>
			  <p><input placeholder="yyyy" oninput="this.className = ''"></p>
			</div>

			<div class="tab">Login Info:
			  <p><input placeholder="Username..." oninput="this.className = ''"></p>
			  <p><input placeholder="Password..." oninput="this.className = ''"></p>
			</div>-->

			<div style="overflow:auto;">
			  <div style="float:right;">
			    <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
			    <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
			  </div>
			</div>
			<button id="setindividualsize" type="button" class="btn btn-outline-secondary pull-right">Setar</button>

			<!-- Circles which indicates the steps of the form: -->
			<div style="text-align:center;margin-top:40px;">
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			</div>

		</form>
	</div>
	
</div>
	
	
@endsection