<div class="row">
	<div class="col-md-6">
		<input type="hidden" name="problem_id" value="{{ $problem->id }}">
		<div class="card  mb-3">
			<h4 class="card-header">Descrição</h4>
			<div class="card-body">
				<div class="form-row">					
					<div class="form-group col-md-12">
						<label for="name">Título</label>
						<input type="text" class="form-control" id="name" name="name">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12">
						<label for="description">Descrição</label>
						<textarea class="form-control" id="description" name="description"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		
		<input type="hidden" name="problem_id" value="{{ $problem->id }}">
		<div class="card  mb-3">
			<h4 class="card-header">População</h4>
			<div class="card-body">
				<div class="form-row">					
					<div class="form-group col-md-6">
						<label for="population_size">Individuos</label>
						<input type="number" name="population_size" class="form-control" placeholder="Quantidade de indivíduos">
					</div>
			
					<div class="form-group col-md-6">
						<label for="individual_size">Cromossomos</label>
						<input type="number" name="individual_size" id="individual_size" class="form-control" placeholder="Quantidade de cromossomos">
					</div>
				</div>
			</div>
			<div class="card-footer text-right">
				<button id="setindividualsize" type="button" class="btn btn-outline-secondary pull-right">Setar</button>
			</div>
		</div>
	</div>
</div>

<!-- <div class="row">
	<div class="col-md-6">
		<div class="card  mb-3">
			<div class="card-header">Configuração da População</div>
			<div class="card-body">
				<label for="population_size">Tamanho</label>
				<input type="number" name="population_size" class="form-control" placeholder="Quantidade de indivíduos">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card  mb-3">
			<div class="card-header">Configuração do Individuo</div>
			<div class="card-body">
				<label for="individual_size">Tamanho</label>
				<input type="number" name="individual_size" id="individual_size" class="form-control" placeholder="Quantidade de cromossomos">
				
			</div>
			<div class="card-footer bg-dark text-right">
				<button id="setindividualsize" type="button" class="btn btn-dark pull-right">Setar</button>
			</div>
		</div>
	</div>
	
</div> -->
<div class="card  mb-3">
	<h4 class="card-header">Configuração do Cromossomo</h4>
	<div class="card-body">
		<div class="form-row">
			<div class="form-group col-md-4">
				<label for="type">Tipo</label>
				<select name="type" class="form-control custom-select">
					<option selected>Selecione</option>
					<option value="1">Inteiro</option>
					<option value="2">Binário</option>
				</select>
			</div>
			<!-- <div class="form-grou col-md-3">
				<label for="chromosome_size">Tamanho</label>
				<input type="number" name="chromosome_size" class="form-control">
			</div> -->
			<div class="form-group col-md-4">
				<label for="chromosome_initial_value">Valor Mínimo</label>
				<input type="number" name="chromosome_initial_value" class="form-control">
			</div>
			<div class="form-group col-md-4">
				<label for="chromosome_final_size">Valor Máximo</label>
				<input type="number" name="chromosome_final_size" class="form-control">
			</div>
		</div>
	</div>
</div>
<div class="card mb-3">
	<h4 class="card-header">Crossover</h4>
	<div class="card-body">
		<div class="row">
			<span class="individualLabel">Individuo A</span>
			<div id="individualA">
			</div>
			
		</div>
		<div class="row">
			<span class="individualLabel">Individuo B</span>
			<div id="individualB">
			</div>
			
		</div>
		
		

		
		

	</div>
</div>
<div class="card  mb-3">
	<h4 class="card-header">Solução</h4>
	<div class="card-body">
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="formula">Função Fitness</label>
				<input type="text" name="formula" id="formula" class="form-control" hidden>
				<input type="text" name="formula-display" id="formula-display" class="form-control" disabled>
			</div>
			<div class="form-group col-md-4">
				<label for="optimal_solution">Solução Ótima</label>
				<input type="text" name="optimal_solution" class="form-control">
			</div>
			<div class="form-group col-md-2">
				<label for="iterations">Iterações</label>
				<input type="number" name="iterations" class="form-control">
			</div>
		</div>
	</div>
			<div class="card-footer">
				<div id="solution_options">
					<div class="row">
						<div id="solution_chromosomes" class="btn-group" role="group" aria-label="Basic example">
							
						</div>						
					</div>
					<div id="solution_operations" class="row">
						<div class="btn-group" role="group" aria-label="Basic example">
							<button type="button" class="btn btn-dark operator" data="+">+</button>
							<button type="button" class="btn btn-dark operator" data="-">-</button>
							<button type="button" class="btn btn-dark operator" data="*">*</button>
							<button type="button" class="btn btn-dark operator" data="/">/</button>
							<button type="button" class="btn btn-dark operator" data="(">(</button>
							<button type="button" class="btn btn-dark operator" data=")">)</button>
						</div>
					</div>
					<div class="row">
						<div class="btn-group" role="group" aria-label="Basic example">
							<button type="button" class="btn btn-dark operator" data="0">0</button>
							<button type="button" class="btn btn-dark operator" data="1">1</button>
							<button type="button" class="btn btn-dark operator" data="2">2</button>
							<button type="button" class="btn btn-dark operator" data="3">3</button>
							<button type="button" class="btn btn-dark operator" data="4">4</button>
							<button type="button" class="btn btn-dark operator" data="5">5</button>
							<button type="button" class="btn btn-dark operator" data="6">6</button>
							<button type="button" class="btn btn-dark operator" data="7">7</button>
							<button type="button" class="btn btn-dark operator" data="8">8</button>
							<button type="button" class="btn btn-dark operator" data="9">9</button>
							
						</div>						
					</div>
				</div>
			</div>
</div>
<div class="text-right">
	<button type="submit" class="btn btn-outline-dark">Criar</button>
</div>
<br>