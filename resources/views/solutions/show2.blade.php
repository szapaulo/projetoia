<table class="table table-striped">
	<thead class="thead-dark">
		<tr>
			<th>Título</th>
			<th>Descrição</th>
		</tr>
	</thead>
	<tbody>
	<tr>
		<td>{{ $solution->name }}</td>
		<td>{{ $solution->description }}</td>
	</tr>
	</tbody>
</table>