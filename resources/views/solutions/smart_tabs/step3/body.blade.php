<div id="step-3" class="solution_step">
	<h3>Avaliação da População</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning" role="alert">
			  @include('solutions.smart_tabs.step2.message')
			</div>
		</div>
		<div class="col-md-3">
			<fieldset>
                <form id="formula_form">

    				<div class="form-row">
    					<div class="form-group col-md-12">
    						<label for="formula">Função Fitness</label>
                            <div class="row">
                                <div class="col-md-10">
                                    <input type="hidden" name="formula" id="formula" class="form-control" value="$solution=">
                                    <input type="text" name="formula-display" id="formula-display" class="form-control form-control-sm" readonly>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-warning btn-sm" id="reset_formula"><i class="fas fa-eraser"></i></button>
                                </div>
                            </div>
                        </div>
    				</div>


                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="operator">Operador</label>
                            <select  name="operator" class="formula_operator form-control form-control-sm">
                                <option selected></option>
                                    <option class="operator">+</option>
                                    <option class="operator">-</option>
                                    <option class="operator">*</option>
                                    <option class="operator">/</option>
                                    <option class="operator">(</option>
                                    <option class="operator">)</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="constant">Constante</label>
                            <select  name="constant" class="formula_operator form-control form-control-sm">
                                <option selected></option>
                                    <option class="operator">0</option>
                                    <option class="operator">1</option>
                                    <option class="operator">2</option>
                                    <option class="operator">3</option>
                                    <option class="operator">4</option>
                                    <option class="operator">5</option>
                                    <option class="operator">6</option>
                                    <option class="operator">7</option>
                                    <option class="operator">8</option>
                                    <option class="operator">9</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="variables">Cromossomos</label>
                            <select  name="variables" id="variables" class="formula_operator form-control form-control-sm">
                                <option selected></option>
                                    <option value="$c[0]" class="variables">C1</option>
                                    <option value="$c[1]" class="variables">C2</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="col-md-12">
            				<button class="btn btn-sm btn-success btn-block mt-2" id="evaluate_population">Aplicar</button>
                        </div>
                    </div>
                </form>


                <hr>

                <form id="avaliation_form">
                    <div class="form-row mt-4">
                        <div class="form-group col-md-8">
                            <label for="avaliation">Método de Avaliação</label>
                            <select name="avaliation" id="avaliation_method" name="selection_method" class="form-control form-control-sm">
                                <option selected>Selecione</option>
                                <option value="1">Crescente</option>
                                <option value="2">Descrescente</option>
                                <option value="3">Aproximação</option>
                            </select>
                        </div>
                        <div id="optimal_solution" class="form-group col-md-4" style="display: none;">
                            <label for="optimal_solution">Valor</label>
                            <input type="text" name="optimal_solution" class="form-control form-control-sm">
                        </div>
                    </div>

                    <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="selection_method">Método de seleção</label>
                                <select id="selection_method" name="selection_method" class="form-control form-control-sm">
                                    <option selected>Selecione</option>
                                    <option value="1">Ranking</option>
                                    <option value="2">Roleta</option>
                                    <option value="3">Torneio</option>
                                </select>
                            </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-12">
                            <button class="btn btn-sm btn-success btn-block mt-2" id="evaluate_population">Avaliar e Selecionar</button>
                        </div>
                    </div>
                </form>
			</fieldset>
		</div>
		<div class="col-md-9">
            <div id="avaliated_population"></div>
		</div>
	</div>
</div>