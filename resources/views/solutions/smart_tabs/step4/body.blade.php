<div id="step-4" class="solution_step">
	<h3>Cruzamento de indivíduos</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning" role="alert">
			  @include('solutions.smart_tabs.step4.message')
			</div>
		</div>
		<div class="col-md-3">
			<fieldset>
				<form id="crossover_form">
					<div class="form-group row">
						<div class="col-sm-12">
							<div class="form-check">
							  <input type="checkbox" class="form-check-input" id="mutation" name="mutation" data-labelauty="Não Aplicar Mutação|Aplicar Mutação">
							  <label class="form-check-label" for="mutation">Aplicar Mutação?</label>
							</div>
						</div>
					</div>

					<hr>

					Pontos de Corte
					<div class="form-row text-center">
						<div class="col-md-6">
							<span class="individualLabel">Ind. 1</span>
							<div id="individualA">
								<div class="row">

								</div>
							</div>

						</div>
						<div class="col-md-6">
							<span class="individualLabel">Ind. 2</span>
							<div id="individualB">
								<div class="row">

								</div>
							</div>

						</div>
							<div class="col-md-12 mt-2 text-center">
								<button class="btn btn-dark btn-sm" id="generate_crossover" type="button">Gerar Aleatóriamente</button>
							</div>
					</div>
					<hr>
					<button class="btn btn-success btn-block btn-sm" id="apply_crossover">Realizar Cruzamento</button>
				</form>
					<hr>
				<form id="generate_new_population_form">
					<label class="form-check-label" for="mutation">Nova População</label>
					<div class="form-check">
					  <input class="form-check-input" type="radio" name="keep_individuals" id="keep_individuals1" value="0" data-labelauty="Apenas Novos Indivíduos" checked>
					  <label class="form-check-label" for="keep_individuals1">Apenas Novos Indivíduos</label>
					</div>
					<div class="form-check">
					  <input class="form-check-input" type="radio" name="keep_individuals" id="keep_individuals2" value="1" data-labelauty="Manter Indivíduos Anteriores">
					  <label class="form-check-label" for="keep_individuals2">Manter Indivíduos Anteriores</label>
					</div>
					<button class="btn btn-success btn-block btn-sm" id="generate_new_population">Gerar Nova População</button>
				</form>
			</fieldset>
		</div>
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-6">
					<h4>Individuos Selecionados</h4>
					<div id="selected_individuals">

					</div>
				</div>
				<div class="col-md-6">
					<h4>Novos Indivíduos Gerados</h4>
					<div id="new_individuals">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>