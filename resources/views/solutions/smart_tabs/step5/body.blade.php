<div id="step-6" class="solution_step">
	<h3>Condição de Parada</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning" role="alert">
			  @include('solutions.smart_tabs.step5.message')
			</div>
		</div>
		<div class="col-md-3">
			<fieldset>
				<form id="finish_form">
					<div class="form-group col-md-12">
						<label for="type">Critério de Parada</label>

						<div class="form-check">
							<input class="form-check-input" type="radio" value="1" id="stop1" name="stop" checked>
							<label class="form-check-label" for="stop1">
								Iterações
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" value="1" id="stop1" name="stop" disabled>
							<label class="form-check-label" for="stop1">
								Solução Ótima
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" value="1" id="stop1" name="stop" disabled>
							<label class="form-check-label" for="stop1">
								Solução Boa
							</label>
						</div>

					</div>
					<div class="form-group col-md-12">
						<label for="iterations">Quantidade de iterações</label>
						<input type="number" name="iterations" class="form-control" min="3" max="15">
					</div>
					<button class="btn btn-success btn-block btn-sm" id="finish">Finalizar</button>
				</form>
			</fieldset>
		</div>
		<div class="col-md-9">
			<div class="row" id="final_result">
			</div>
		</div>
	</div>
</div>