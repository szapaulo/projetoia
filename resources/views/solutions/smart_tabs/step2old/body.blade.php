<div id="step-2" class="solution_step">
	<br>
	<h3>Gerar População inicial</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning" role="alert">
			  @include('solutions.smart_tabs.step2.message')
			</div>
		</div>
		<div class="col-md-3">
			<fieldset>
			<div class="form-row">
				<div class="form-group col-md-12">
					<label for="type">População Inicial</label>
					<select name="type" class="form-control custom-select">
						<option selected>Selecione</option>
						<option value="1">Aleatório</option>
						<option value="2">Entrada</option>
					</select>
				</div>
			</div>
			<button class="btn btn-info btn-block btn-lg" id="create_initial_population">Gerar População Inicial</button>
			</fieldset>
		</div>
		<div class="col-md-9" id="initial_population">
            <table id="mytable">
                <tr>
                    <td class="individual_number">Indivíduo 1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td class="individual_number">Indivíduo 2</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="individual_number">Indivíduo 3</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="individual_number">Indivíduo 4</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="individual_number">Indivíduo 5</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td class="individual_number">Indivíduo 6</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="individual_number">Indivíduo 7</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td class="individual_number">Indivíduo 8</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="individual_number">Indivíduo 9</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="individual_number">Indivíduo 10</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                </tr>
            </table>
		</div>
	</div>

</div>