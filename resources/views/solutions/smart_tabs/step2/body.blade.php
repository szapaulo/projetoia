
<div id="step-2" class="solution_step">
	<br>
	<h3>Configurações Iniciais do Algoritmo Genético</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning" role="alert">
			  @include('solutions.smart_tabs.step1.message')
			</div>
		</div>
		<div class="col-md-3">
			<fieldset>
				<form id="create_solution_form">
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="population_size">Nº Individuos</label>
							<div class="input-group">
		                        <div class="input-group-prepend">
		                            <button type="button" class="btn btn-sm btn-success" id="dec_pop"><i class="fas fa-minus"></i></button>
		                        </div>
								<input type="number" name="population_size" id="population_size" class="form-control form-control-sm" placeholder="Quantidade de indivíduos" value="2" readonly>
								<div class="input-group-append">
		                            <button type="button" class="btn btn-sm btn-success" id="inc_pop"><i class="fas fa-plus"></i></button>
		                        </div>
		                    </div>
						</div>
						<div class="form-group col-md-6">
							<label for="individual_size">Nº Cromossomos</label>
							<div class="input-group">
		                        <div class="input-group-prepend">
		                            <button type="button" class="btn btn-sm btn-success" id="dec_ind"><i class="fas fa-minus"></i></button>
		                        </div>
								<input type="number" name="individual_size" id="individual_size" class="form-control form-control-sm" placeholder="Quantidade de cromossomos" value="2" readonly>
								<div class="input-group-append">
		                            <button type="button" class="btn btn-sm btn-success" id="inc_ind"><i class="fas fa-plus"></i></button>
		                        </div>
		                    </div>
						</div>
					</div>

					<button class="btn btn-success btn-block btn-sm" id="create_solution" type="submit">Definir Configurações</button>
				</form>

				<hr>

				<div class="form-row">
					<div class="form-group col-md-12 mt-2">
						Gerar automaticamente: <button class="btn btn-success btn-sm" id="generate_population" type="button" disabled><i class="fas fa-dice-three"></i></button>
					</div>
				</div>
				<button class="btn btn-success btn-block btn-sm" id="submit_initial_population" type="button" disabled>Salvar População Inicial</button>

			</fieldset>
		</div>
		<div class="col-md-9" id="initial_population">
			<form id="create_initial_population">
	            <table id="mytable">
	                <tr>
	                    <td class="i-1 c-1"><input class="chromosome_value" id="c-1-1" type="number" name="population[1][1]"></td>
	                    <td class="i-1 c-2"><input class="chromosome_value" id="c-1-2" type="number" name="population[1][2]"></td>
	                </tr>
	                <tr>
	                    <td class="i-2 c-1"><input class="chromosome_value" id="c-2-1" type="number" name="population[2][1]"></td>
	                    <td class="i-2 c-2"><input class="chromosome_value" id="c-2-2" type="number" name="population[2][2]"></td>
	                </tr>
	            </table>
            </form>
		</div>
	</div>

</div>