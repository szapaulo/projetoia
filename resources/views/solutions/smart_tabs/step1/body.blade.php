<div id="step-1" class="solution_step">

	<br>

	<h3>Selecionar um Problema</h3>

	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning" role="alert">
			  @include('solutions.smart_tabs.step1.message')
			</div>
		</div>
		<div class="col-md-3">
			<form id="problem_form">
				<fieldset>
					<div class="form-row">
						<div class="form-group col-md-12">
							@include('problems.partials.problem_selection')
						</div>

					</div>
					<button class="btn btn-success btn-block btn-sm" id="set_problem" type="submit">Salvar</button>
				</fieldset>
			</form>

		</div>
		<div class="col-md-9" id="problem_details">

		</div>
	</div>

</div>