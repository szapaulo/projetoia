<div class="help-message">
O primeiro passo na implementação do algoritmo genético é definir as configurações iniciais do algorítimo. <i class="help-icon far fa-question-circle" title="Ajuda"></i>

<div class="full-helper text-center">
    <div class="row justify-content-center h-100">
        <div class="col-md-4 my-auto">
            <div class="card">
                <div class="card-body">
                    <p>
                        O primeiro passo na implementação do algoritmo genético é definir as configurações iniciais do algorítimo.
                    </p>

                    <button type="button" class="btn btn-success close-helper">Entendi</button>
                </div>
            </div>
        </div>
    </div>
</div>

</div>