@extends('layouts.app')
@php
	$subtitle = '';
	$title = "AMBIENTE VIRTUAL DE TREINAMENTO EM ALGORITMOS GENÉTICOS";
@endphp
@section('content')
<div class="row text-success justify-content-md-center mt-2">
	<div class="col-md-12 text-center">
		<h5>Técnica de IA que replica os princípios da seleção natural, onde indivíduos mais adaptados possuem mais chances de sobrevivência.</h5>
	</div>

</div>

	<div class="row mt-5">
		<div class="col-md-6">
			<div class="card border-dark bg-transparent">
				<div class="card-body">
			<h4>Inspiração Biológica </h4>
			<p>Na natureza, os indivíduos se recombinam através da reprodução gerando populações com diversidades de características e até novos atributos produtos da recombinação.</p>
			<p>No algoritmo, os indivíduos são soluções de um problema específico em um domínio de todas as soluções possíveis. E busca-se pela solução ótima através de combinações entre soluções.</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card bg-transparent border-dark h-100">
				<div class="card-body">
			<h4>Representação dos Parâmetros</h4>
			<ul>
				<li>Determinar a configuração de uma possível solução;</li>
				<li>Identificar e delimitar o conjunto domínio em que constam as soluções;</li>
				<li>Cromossomos: conjunto de símbolos que representam uma possível solução.</li>
			</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row justify-content-md-center mt-5">
	<div class="col-md-6 col-md-offset-3">
		<a href="{{ route('main.menu') }}" class="btn btn-outline-success btn-block btn-lg">Iniciar</a>
	</div>
	</div>



@endsection