<!DOCTYPE html>
<html lang="en" class="h-100">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/smart_wizard.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/smart_wizard_theme_circles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/smart_wizard_theme_dots.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/smart_wizard_theme_arrows.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/labelauty/jquery-labelauty.css') }}">
  </head>
  <body class="h-100 ">
    <div class="overlay text-center">
      <div class="row justify-content-center h-100">
        <div class="col-md-4 my-auto">
          <img src="{{ asset('img/loading.gif') }}" class="my-auto" alt="">
        </div>
      </div>
    </div>
    <div id="header">
      <div class="container-fluid">
      <div class="row justify-content-md-center">
        <div class="col-md-4 align-items-center text-center">

        <div class="dna clearfix d-inline-block">
          <div class="hex_left"></div>
          <div class="hex_center">
            <i class="logo fas fa-dna"></i>
          </div>
          <div class="hex_right"></div>
        </div>


        </div>
      </div>
    </div>

      </div>

    <div class="container-fluid h-80">
      <div class="row h-100">
        <main class="col-sm-9 ml-sm-auto col-md-12 my-auto" role="main">

            @yield('content')

        </main>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-3.1.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/ia.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.smartWizard.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/sweetalert2.all.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/labelauty/jquery-labelauty.js') }}"></script>
            @yield('scripts')
  </body>
</html>