@extends('layouts.app')
@section('content')
	<div class="row justify-content-md-center pt-20">
		<div class="col-md-4">
			<a href="{{ route('teoria') }}" class="btn btn-block btn-lg btn-outline-success">TEORIA</a>
		</div>
		<div class="col-md-4">
			<a href="" class="btn btn-block btn-lg btn-outline-success">EXEMPLOS</a>
		</div>
	</div>

	<div class="row justify-content-md-center mt-5">
		<div class="col-md-4">
			<a href="{{ route('new_ag') }}" class="btn btn-block btn-lg btn-outline-success">ADD AG</a>
		</div>
		<div class="col-md-4">
			<a href="#" class="btn btn-block btn-lg btn-outline-success">VISUALIZAR ALGO</a>
		</div>
	</div>
@endsection