<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solutions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('formula_id')->unsigned()->nullable();
            $table->integer('mutation_id')->unsigned()->nullable();
            $table->integer('population_size')->nullable();
            $table->integer('individual_size')->nullable();
            $table->string('formula')->nullable();
            $table->string('formula_view')->nullable();
            $table->integer('solution')->nullable();
            $table->boolean('keep_individuals')->nullable();
            $table->integer('avaliation_method')->nullable();
            $table->integer('selection_method')->nullable();
            $table->string('optimal_solution')->nullable();
            $table->integer('chromosome_initial_value')->nullable();
            $table->integer('chromosome_final_size')->nullable();
            $table->integer('iterations')->nullable();
            $table->integer('chromosome_size')->nullable();
            $table->string('crossover')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();

            // $table->foreign('formula_id')->references('id')->on('formulas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solutions');
    }
}
