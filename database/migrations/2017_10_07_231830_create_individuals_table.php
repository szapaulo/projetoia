<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndividualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individuals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fitness_solution')->nullable();
            $table->integer('created_at_generation')->unsigned();
            $table->integer('performance')->unsigned()->nullable();
            $table->integer('index')->unsigned()->nullable();
            $table->integer('parent1_id')->unsigned()->nullable();
            $table->integer('parent2_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('individuals');
    }
}
