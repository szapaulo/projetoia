<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntChromosomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('int_chromosomes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('individual_id')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('value');
            $table->boolean('mutated')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('int_chromosomes');
    }
}
