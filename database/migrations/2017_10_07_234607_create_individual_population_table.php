<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndividualPopulationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individual_population', function (Blueprint $table) {
            $table->integer('individual_id')->unsigned();
            $table->integer('population_id')->unsigned();
            $table->integer('index')->unsigned()->nullable();
            $table->integer('rank')->unsigned()->nullable();
            $table->float('performance', 8, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('individual_population');
    }
}
