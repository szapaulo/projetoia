<?php

namespace App\Operators;

use Illuminate\Http\Request;
use App\Models\Solution;
use App\Models\Problem;
use App\Models\Individual;
use App\Models\IntChromosome;
use App\Models\Population;
use App\Models\Formula;
use App\Models\Resolution;

class Selection
{

	private $population;

	const RANKING = 1;
	const ROULETTE_WHEEL = 2;
	const TOURNAMENT = 3;

	//Ranking

	public static function select(Population $population, $method)
	{
		switch ($method) {
			case 1:
				return self::ranking($population);
				break;

			case 2:
				return self::rouletteWheel($population);
				break;

			case 3:
				return self::tournament($population, 2);
				break;

			default:
				// code...
				break;
		}
	}

	public static function ranking(Population $population)
	{
		$individuals = $population->individuals;

		$pop_size = $individuals->count();
		$selection_size = (round(sqrt($pop_size)) + 1);

		$selected = array();
		for ($i = 0; $i < $selection_size; $i++) {
		// SUM ALL INDIVIDUALS RANKS
		$sum = $individuals->sum('pivot.rank');
		$rand = rand(0, $sum);
		$aux = 0;
			foreach ($individuals as $key => $individual) {
				$aux += $individual->pivot->rank;
				if ($rand <= $aux) {
				    $selected[] = $individual->id;
				    unset($individuals[$key]);
				    break;
				}
			}
		}
		return $selected;
	}

	public static function rouletteWheel(Population $population)
	{
		$individuals = $population->individuals;

		
		$sum_fitness = $individuals->sum('fitness_solution');
		$avaliation_method = $population->resolution->solution->avaliation_method;

		switch ($avaliation_method) {
            case 1:
		        foreach ($individuals as $individual) {
		            $performance = $individual->fitness_solution / $sum_fitness;
		            $population->individuals()->updateExistingPivot($individual->id, ['performance' => $performance]);
		        }
		        break;
            case 2:
                foreach ($this->individuals as $individual) {
                    $performance = 1 / (($individual->fitness_solution / $sum_fitness) + 1);
		            $population->individuals()->updateExistingPivot($individual->id, ['performance' => $performance]);
                }
                break;
            case 3:
                foreach ($this->individuals as $individual) {
                    $performance = 1 / ($individual->performance + 1);
		            $population->individuals()->updateExistingPivot($individual->id, ['performance' => $performance]);
                }
                break;
        }	

		$pop_size = $individuals->count();
		$selection_size = (round(sqrt($pop_size)) + 1);

		$selected = array();
		for ($i = 0; $i < $selection_size; $i++) {
		// SUM ALL INDIVIDUALS PERFORMANCES
		$sum = $population->individuals->whereNotIn('id', $selected)->sum('pivot.performance');
		$rand = rand(0, $sum*100) / 100;
		$aux = 0;
			foreach ($individuals as $key => $individual) {
				$aux += $individual->pivot->performance;
				if ($rand <= $aux) {
				    $selected[] = $individual->id;
				    unset($individuals[$key]);
				    break;
				}
			}
		}
		// dd($selected);
		return $selected;
	}

	public function tournament(Population $population, $k)
	{

		$individuals = $population->individuals;

		$pop_size = $individuals->count();
		$selection_size = (round(sqrt($pop_size)) + 1);

		$selected = array();
		for ($i = 0; $i < $selection_size; $i++) {

			if($individuals->count() < $k) {
				$select = $individuals->first()->id;
			} else {
				// SELECT K INDIVIDUALS
				$i = $individuals->random($k);

				// GET THE BETTER INDIVIDUAL
				$select = ($i[0]->pivot->rank >= $i[1]->pivot->rank)? $i[0]->id: $i[1]->id;
			}

			$selected[] = $select;
			$key = $individuals->where('id', $select)->keys()->first();
			unset($individuals[$key]);
		}
		return $selected;
	}
}