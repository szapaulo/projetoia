<?php

namespace App\Operators;

use Illuminate\Http\Request;
use App\Models\Solution;
use App\Models\Problem;
use App\Models\Individual;
use App\Models\IntChromosome;
use App\Models\Population;
use App\Models\Formula;
use App\Models\Resolution;

class Mutation
{
	public static function mutate(Population $population)
	{
		$qty = $population->individuals->first()->IntChromosomes()->count();
		foreach ($population->individuals as $individual) {
			$apply_mutation = rand(0,1);
			if ($apply_mutation) {
				$index = rand(0,$qty-1);
				$chromosome = $individual->IntChromosomes[$index];
				$chromosome->value = !$chromosome->value;
				$chromosome->mutated = true;
				$chromosome->save();
			}
			
		}

	}
}