<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Solution;
use App\Models\Problem;
use App\Models\Individual;
use App\Models\IntChromosome;
use App\Models\Population;
use App\Models\Formula;
use App\Models\Resolution;
use App\Operators\Selection;
use App\Operators\Mutation;

class SolutionController extends Controller
{

  public function show($id){

    $solution = Solution::find($id);
    // $resolutions = solution->resolutions;
    // dd($solutions);
    return view('solutions.show', ['solution' => $solution]);

  }

  public function ajaxCreate(Request $request)
  {
    // dd($request);
    // $problem_id = $request->problem_id;
    // $request = $request->dados;
    // dd('$request');
    // dd($request->problem_id);
    $new_solution = Solution::findOrNew($request->solution_id);

    $new_solution->population_size = $request->population_size;
    $new_solution->individual_size = $request->individual_size;
    $new_solution->chromosome_initial_value = 0;
    $new_solution->chromosome_final_size = 1;
    $new_solution->problem_id = $request->problem_id;
    $new_solution->user_id = 2;
    // $new_solution->user_id = \Auth::id();
    $new_solution->type = $request->type;
    $new_solution->save();

    return response()->json($new_solution->toArray());

    // echo "string";
    $solution = Solution::updateOrCreate($request->solution, $request->form);
    // $solution = Solution::create($request->form);
    return $solution->id;
    // $p = Problem::find(1);
    // var_dump($p);
    // return Response::json($task);
  }

  public function ajaxInitialPopulation(Request $request)
  {
    // dd($request);
    $solution = Solution::find($request->solution_id);

    $new_resolution = new Resolution();
    $new_resolution->user_id = 2;
    // $new_resolution->user_id = \Auth::id();
    $solution->resolutions()->save($new_resolution);


    $new_population = new Population();
    $new_population->generation = 0;
    $new_resolution->populations()->save($new_population);

    foreach ($request->population as $key => $individual) {
      $new_individual = new Individual();
      $new_individual->created_at_generation = 0;
      $new_individual->save();
      $new_population->individuals()->attach($new_individual->id, ['index' => $key]);

      foreach ($individual as $key => $chromosome) {
        $new_chromosome = new IntChromosome();
        $new_chromosome->value = $chromosome;
        $new_individual->intChromosomes()->save($new_chromosome);
      }
    }
    return response()->json($new_population->load('individuals.intChromosomes')->toArray());
    return response()->json($new_resolution->load('populations.individuals.intChromosomes')->toArray());
  }

  public function ajaxSaveFormula(Request $request)
  {
    // dd($request);
    $solution = Solution::find($request->solution_id);
    $solution->formula = $request->formula;
    $solution->formula_view = $request['formula-display'];
    $solution->save();

    $population = $solution->resolutions()->orderBy('id', 'desc')->first()->populations()->orderBy('id', 'desc')->first();
    // dd($population);
    $population->evaluate();
    // dd($population);
    return View('populations.partials.formula_result', compact('population', 'solution'));
  }


// 1 - CRESCENTE
// 2 - DECRESCENTE
// 3 - APROXIMAÇÃO
  public function ajaxAvaliate(Request $request)
  {
    // dd($request);
    $solution = Solution::find($request->solution_id);
    $solution->avaliation_method = $request->avaliation;
    $solution->selection_method = $request->selection_method;
    $solution->optimal_solution = $request->optimal_solution;
    $solution->save();
    $population = $solution->resolutions()->orderBy('id', 'desc')->first()->populations()->orderBy('id', 'desc')->first();
    $population->avaliate($request->avaliation);
    $selected = Selection::select($population, $request->selection_method);
    $population->selected = implode(',', $selected);
    $population->save();

    // dd($selected);

    // $population = $solution->resolutions()->first()->populations()->first();
    // switch ($request->avaliation) {
    //   case 1:
    //     $individuals = $population->individuals()->orderBy('performance', 'desc')->get();
    //     break;
    //   case 2:
    //     $individuals = $population->individuals()->orderBy('fitness_solution', 'desc')->get();
    //     break;
    //   case 3:
    //     $individuals = $population->individuals()->orderBy('performance')->get();
    //     break;
    // }

    // $individuals = $population->individuals()->orderBy('pivot_rank', 'desc')->get();
    $response['view'] = View('populations.partials.avaliated', compact('population', 'solution', 'selected'))->render();
    $response['selected'] = $selected;

    // $selected_individuals = $individuals->whereIn('id', $selected);
    // $selected_individuals = $individuals;

    $response['selected_individuals'] = View('populations.partials.selected', compact('population', 'selected'))->render();

    // dd($individuals->toArray());
    // $individuals = $population->individuals()->orderBy('fitness_solution')->get();
    // dd($individuals->get());
        // $response['result'] = View('order.partials.list', compact('orders'))->render();
        // $valor_inicial = ($orders->currentPage() - 1) * $orders->perPage() + 1;
        // $valor_final = ($orders->currentPage() - 1) * $orders->perPage() + $orders->count();
        // $filter_total = $orders->total();
        // $response['result_info'] = 'Mostrando de '. $valor_inicial .' até '. $valor_final .' de '. $filter_total .' registros (Filtrados de '. $total_no_filter .' registros)';
        // return $response;
    return $response;
  }


  public function ajaxCrossover(Request $request)
  {
    $crossover = $request->chromo;
    ksort($crossover);
    // dd($request->mutation);
    // dd($crossover);
    $solution = Solution::find($request->solution_id);
    $solution->crossover = implode(',', $crossover);
    $solution->mutation_id = $request->mutation? 1: 0;
    $solution->save();

    $resolution = $solution->resolutions()->orderBy('id', 'desc')->first();
    $population = $resolution->populations()->orderBy('id', 'desc')->first();
    // dd($population);

    $selected = explode(',', $population->selected);

    $selected_individuals = $population->individuals()->with('intChromosomes')->whereIn('id', $selected)->get()->toArray();

    // dd($selected_individuals[0]['int_chromosomes']);
    // dd(count($selected_individuals));


    // dd($population);
    $populationsize = $solution->population_size;
    $individualsize = $solution->individual_size;
    // dd($crossover);
// $resolution->populations()->where('generation', 1)->first()->delete();
    $newPopulation = new Population();
    $newPopulation->generation = 1;
    $resolution->populations()->save($newPopulation);
    $index = 1;
      // dd($populationsize);
      for ($i = 0; $i < (count($selected_individuals) - 1); $i++) {
        if ($newPopulation->individuals()->count() >= $populationsize) break;

          for ($j = ($i+1); $j < count($selected_individuals); $j++) {
            if ($newPopulation->individuals()->count() >= $populationsize) break;

            $new_individual1 = new Individual();
            $new_individual1->created_at_generation = 1;
            $new_individual1->parent1_id = $selected_individuals[$i]['id'];
            $new_individual1->parent2_id = $selected_individuals[$j]['id'];
            $new_individual1->save();
            $newPopulation->individuals()->attach($new_individual1->id, ['index' => $index]);
            $index++;
            if ($newPopulation->individuals()->count() < $populationsize){
              $new_individual2 = new Individual();
              $new_individual2->created_at_generation = 1;
              $new_individual2->parent1_id = $selected_individuals[$j]['id'];
              $new_individual2->parent2_id = $selected_individuals[$i]['id'];
              $new_individual2->save();
              $newPopulation->individuals()->attach($new_individual2->id, ['index' => $index]);
              $index++;
            }
            for ($k = 0; $k < $individualsize; $k++) {
              $new_chromosome1 = new IntChromosome();
              if ($new_individual2)
                $new_chromosome2 = new IntChromosome();
              if ($crossover[$k]) {
                $new_chromosome1->value = $selected_individuals[$i]['int_chromosomes'][$k]['value'];
                $new_chromosome1->parent_id = $selected_individuals[$i]['int_chromosomes'][$k]['id'];
                if ($new_individual2) {
                    $new_chromosome2->value = $selected_individuals[$j]['int_chromosomes'][$k]['value'];
                    $new_chromosome2->parent_id = $selected_individuals[$j]['int_chromosomes'][$k]['id'];
                  }
              }
              else {
                $new_chromosome1->value = $selected_individuals[$j]['int_chromosomes'][$k]['value'];
                $new_chromosome1->parent_id = $selected_individuals[$j]['int_chromosomes'][$k]['id'];
                if ($new_individual2) {
                    $new_chromosome2->value = $selected_individuals[$i]['int_chromosomes'][$k]['value'];
                    $new_chromosome2->parent_id = $selected_individuals[$i]['int_chromosomes'][$k]['id'];
                  }
              }
              $new_individual1->intChromosomes()->save($new_chromosome1);
              if ($new_individual2)
                $new_individual2->intChromosomes()->save($new_chromosome2);
            }
            $new_individual2 = null;
          }

      }
      if($solution->mutation_id)
        Mutation::mutate($newPopulation);
    // dd($newPopulation);
      $newPopulation->evaluate();
    $newPopulation->avaliate($solution->avaliation_method);
    // $selected = Selection::select($newPopulation, $solution->selection_method);
    // $newPopulation->selected = implode(',', $selected);
    // $newPopulation->save();
    // $newPopulation->selected = implode(',', $selected);
    // $newPopulation->save();
    // $individuals = $newPopulation->individuals()->orderBy('pivot_rank', 'desc')->get();
    $population = Population::find($newPopulation->id);
    // $population->setRelations([]);

    // dd($population->individuals()->orderBy('pivot_rank', 'desc')->get()->pluck('fitness_solution', 'id'));
$response['view'] = View('populations.partials.avaliated', compact('population', 'solution'))->render();
return $response;
      return $newPopulation;
  }


  public function ajaxGenerateNewPopulation(Request $request)
  {
    // dd($request->mutation);
    $solution = Solution::find($request->solution_id);
    $avaliation_method = $solution->avaliation_method;
    $selection_method = $solution->selection_method;
    $population_size = $solution->population_size;

    $solution->keep_individuals = $request->keep_individuals;
    // $solution->mutation_id = $request->mutation;
    $solution->save();
      $resolution = $solution->resolutions()->first();
      $populations = $resolution->populations()->orderBy('id', 'desc')->get();

    if ($solution->keep_individuals) {
      $pop1 = $populations[1];
      $pop2 = $populations[0];
      $new_pop = $pop2->individuals->merge($pop1->individuals);
      // dd($pop2);
      // $individuals = $new_pop->sortByDesc('fitness_solution');

      switch ($avaliation_method) {
        case '1':
            $individuals = $new_pop->sortByDesc('fitness_solution');
          break;
        case '2':
            $individuals = $new_pop->sortBy('fitness_solution');
          break;
        case '3':
            $individuals = $new_pop->sortBy('performance');
          break;
      }
      // dd($individuals->pluck('fitness_solution', 'id'));
      // $individuals-
      $top_individuals = $individuals->take($population_size)->pluck('id');
// dd($top_individuals);
      $pop2->individuals()->sync($top_individuals);
// dd($pop2->individuals()->pluck('fitness_solution', 'id'));
      // $pop2->evaluate();
      // $individuals = $pop2->individuals()->orderBy('fitness_solution', 'desc')->orderBy('id', 'desc')->get();
      $newPopulation = Population::with('individuals')->find($pop2->id);
      // dd($individuals->pluck('fitness_solution', 'id'));
      foreach ($newPopulation->individuals as $key => $individual) {
        $newPopulation->individuals()->updateExistingPivot($individual->id, ['index' => $key+1]);
      }

      $newPopulation->avaliate($solution->avaliation_method);

    } else {
      $newPopulation = $pop2;
    }

    $selected = Selection::select($newPopulation, $solution->selection_method);
    $newPopulation->selected = implode(',', $selected);
    $newPopulation->save();
    $population = $newPopulation;
    $population->setRelations([]);
      $response['view'] = View('populations.partials.avaliated', compact('population', 'solution'))->render();
      return $response;
  }

  public function ajaxFinish(Request $request)
  {
    // dd($request);

    $solution = Solution::find($request->solution_id);
    $solution->iterations = $request->iterations;
    $solution->save();

    $resolution = $solution->resolutions()->first();
    $last_population = $resolution->populations()->orderBy('id', 'desc')->first();


    $populationsize = $solution->population_size;
    $individualsize = $solution->individual_size;
    $crossover = explode(',' ,$solution->crossover);
    $avaliation_method = $solution->avaliation_method;
    $selection_method = $solution->selection_method;
    $keep_individuals = $solution->keep_individuals;

    for ($l=2; $l <= $solution->iterations; $l++) {

      $selected = explode(',', $last_population->selected);
      $selected_individuals = $last_population->individuals()->with('intChromosomes')->whereIn('id', $selected)->get()->toArray();

      $newPopulation = new Population();
      $newPopulation->generation = $l;
      $resolution->populations()->save($newPopulation);

      for ($i = 0; $i < (count($selected_individuals) - 1); $i++) {
        if ($newPopulation->individuals()->count() >= $populationsize) break;

          for ($j = ($i+1); $j < count($selected_individuals); $j++) {
            if ($newPopulation->individuals()->count() >= $populationsize) break;

            $new_individual1 = new Individual();
            $new_individual1->created_at_generation = $l;
            $new_individual1->parent1_id = $selected_individuals[$i]['id'];
            $new_individual1->parent2_id = $selected_individuals[$j]['id'];
            $new_individual1->save();
            $newPopulation->individuals()->attach($new_individual1->id);

            if ($newPopulation->individuals()->count() < $populationsize){
              $new_individual2 = new Individual();
              $new_individual2->created_at_generation = 1;
              $new_individual2->parent1_id = $selected_individuals[$j]['id'];
              $new_individual2->parent2_id = $selected_individuals[$i]['id'];
              $new_individual2->save();
              $newPopulation->individuals()->attach($new_individual2->id);
            }
            for ($k = 0; $k < $individualsize; $k++) {
              $new_chromosome1 = new IntChromosome();
              if ($new_individual2)
                $new_chromosome2 = new IntChromosome();
              if ($crossover[$k]) {
                $new_chromosome1->value = $selected_individuals[$i]['int_chromosomes'][$k]['value'];
                $new_chromosome1->parent_id = $selected_individuals[$i]['int_chromosomes'][$k]['id'];
                if ($new_individual2) {
                    $new_chromosome2->value = $selected_individuals[$j]['int_chromosomes'][$k]['value'];
                    $new_chromosome2->parent_id = $selected_individuals[$j]['int_chromosomes'][$k]['id'];
                  }
              }
              else {
                $new_chromosome1->value = $selected_individuals[$j]['int_chromosomes'][$k]['value'];
                $new_chromosome1->parent_id = $selected_individuals[$j]['int_chromosomes'][$k]['id'];
                if ($new_individual2) {
                    $new_chromosome2->value = $selected_individuals[$i]['int_chromosomes'][$k]['value'];
                    $new_chromosome2->parent_id = $selected_individuals[$i]['int_chromosomes'][$k]['id'];
                  }
              }
              $new_individual1->intChromosomes()->save($new_chromosome1);
              if ($new_individual2)
                $new_individual2->intChromosomes()->save($new_chromosome2);
            }
            $new_individual2 = null;
          }
      }

      $newPopulation = Population::with('individuals.intChromosomes')->find($newPopulation->id);
      if($solution->mutation_id)
        Mutation::mutate($newPopulation);

      $newPopulation->evaluate();

      if ($keep_individuals) {
        $new_pop = $newPopulation->individuals->merge($last_population->individuals);

        switch ($avaliation_method) {
          case '1':
              $individuals = $new_pop->sortByDesc('fitness_solution');
            break;
          case '2':
              $individuals = $new_pop->sortBy('fitness_solution');
            break;
          case '3':
              $individuals = $new_pop->sortBy('performance');
            break;
        }
        $top_individuals = $individuals->take($populationsize)->pluck('id');

        $newPopulation->individuals()->sync($top_individuals);
      }


      $newPopulation->setRelations([]);
      foreach ($newPopulation->individuals as $key => $individual) {
        $newPopulation->individuals()->updateExistingPivot($individual->id, ['index' => $key+1]);
      }

      $newPopulation->avaliate($avaliation_method);
      $newPopulation = Population::with('individuals')->find($newPopulation->id);
      $selected = Selection::select($newPopulation, $selection_method);
      $newPopulation->selected = implode(',', $selected);
      $newPopulation->save();

      $last_population = $newPopulation;

    }//iterations

      $response['view'] = View('populations.partials.resolution', compact('resolution'))->render();
      return $response;
  }


  public function ajaxCreatePopulation(Request $request)
  {
    echo "string";
    $solution = Solution::find($request->solution);
    $res = Resolution::create(["solution_id" => $solution->id, "user_id" => "1"]);
    // $solution = Solution::create($request->form);
    return $res->id;
    // $p = Problem::find(1);
    // var_dump($p);
    // return Response::json($task);
  }
  public function createPopulation($options)
    {
      $individual = array();
      $chromossome = array();
      $populationsize = $options['populationsize'];
      $individualsize = $options['individualsize'];
      $chromosomesize = $options['chromosomesize'];
      $minval = $options['minval'];
      $maxval = $options['maxval'];

      for ($i=0; $i < $populationsize; $i++) {
        for ($j = 0; $j < $individualsize; $j++) {
          $number = rand($minval,$maxval);
          $chromossome[] = sprintf('%0'.$chromosomesize.'d',$number);
        }
        $individual['chromossomes'] = $chromossome;
        $population[] = $individual;
        $chromossome = array();
      }

      // foreach ($population as $key => $ind) {
      //   echo '<br>';
      //   echo 'Individuo '.++$key.' => ';
      //   foreach ($ind as $cro) {
      //     // sprintf('%04d |',$cro);
      //     echo $cro.' | ';
      //   }
      // }
      // var_dump($teste);
      return $population;
      dd($population);
      // print_r($population);
      // print_r($teste->pos1);
    }

    public function array_msort($array, $cols)
    {
        $colarr = array();
        foreach ($cols as $col => $order) {
            $colarr[$col] = array();
            foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\''.$col.'\'],'.$order.',';
        }
        $eval = substr($eval,0,-1).');';
        eval($eval);
        $ret = array();
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k,1);
                if (!isset($ret[$k])) $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }
        foreach ($ret as $individual) {
          $rankedPopulation[] = $individual;
        }
        // dd($rankedPopulation);
        return $rankedPopulation;

    }


	public function index(){
    	$solutions = Solution::get();

    	return view('solutions.list', ['solutions' => $solutions]);
    }

    public function create(Request $request)
    {
    	$problem_id = $request->all()['problem_id'];

    	// dd($problem_id);

    	$problem = Problem::find($problem_id);
    	// echo $problem_id;
    	return view('solutions.create', ['problem' => $problem]);
    }

    public function store(Request $request)
    {
      // $solucao = new Solution();
      // $solucao->user_id = 1;
      // $solucao->formula_id = 1;
      // $solucao->mutation_id = 1;
      // $solucao->name = $request->name;
      // $solucao->description = $request->description;
      // $solucao->population_size = $request->population_size;
      // $solucao->individual_size = $request->individual_size;
      // $solucao->type = $type->type;
      // $solucao->chromosome_initial_value = $request->chromosome_initial_value;
      // $solucao->chromosome_final_size = $request->chromosome_final_size;
      // $solucao->formula = $request->formula;
      // $solucao->optimal_solution = $request->optimal_solution;
      // $solution = $solucao->save();

    	$solution = $request->all();
    	$solution['user_id'] = 1;
    	$solution['formula_id'] = 1;
    	$solution['mutation_id'] = 1;
    	$solution = Solution::create($solution);
    	dd($solution->id);

    	$problem_title = Solution::find($solution->id)->problem->name;
    	echo $problem_title;
    }

    public function applyFitness($individual)
    {
      $fitness = $individual['fitness'];
      $chromossomes = $individual['chromossomes'];
      eval($fitness);
      return $solution;
    }




    public function calculate(Request $request)
    {



      // Transforma o crossoverem um array (1-individuo 1, 0-individuo 2)
      $crossover = "";
      for ($i = 0; $i < $request->individual_size; $i++) {
        $crossover .= $request["chromo".$i];
      }
      // dd($request->all());

      $formula = new Formula();

      // $formula->user_id = 1;
      // $formula->type = 1;
      // $formula->formula = $request->formula;
      // $formula->save();

      // dd($formula->id);

      $solucao = new Solution();
      $solucao->user_id = 1;
      // $solucao->formula_id = $formula->id;
      $solucao->mutation_id = 1;
      $solucao->name = $request->name;
      $solucao->description = $request->description;
      $solucao->population_size = $request->population_size;
      $solucao->individual_size = $request->individual_size;
      $solucao->chromosome_size = $request->chromossome_size;
      $solucao->type = $request->type;
      $solucao->chromosome_initial_value = $request->chromosome_initial_value;
      $solucao->chromosome_final_size = $request->chromosome_final_size;
      $solucao->optimal_solution = $request->optimal_solution;
      $solucao->problem_id = $request->problem_id;
      $solucao->crossover = $crossover;
      $solution = $solucao->save();
      return redirect()->route('solucoes.show', $solucao->id);
      dd($solution);

      // dd($crossover);
      // $solucao->formula = $request->formula;

    	// $data = $request->all();
      // dd($data);
    	// $solution = new Solution();
    	// $solution.name="oi";
    	// dd($solution);

      // Seta as configurações da população
      $populationConfigs = array(
        'populationsize' => $request->population_size,
        'individualsize' => $request->individual_size,
        'chromosomesize' => $request->chromosome_size,
        'minval' => $request->chromosome_initial_value,
        'maxval' => $request->chromosome_final_size,
        'crossover' => $crossover
      );

      $iterations = $request->iterations;

      // Seta as configurações da solução
      $fitnessSolutionConfigs = array(
        'formula' => $request->formula,
        'optimal_solution' => $request->optimal_solution
      );

      // Gera a População inicial
      $initialPopulation = $this->createPopulation($populationConfigs);

      // $populations[] = $this->createPopulation($populationConfigs);
      // dd($initialPopulation);
      // foreach ($populations as $population) {
      // }

      // $populations[] = $initialPopulation;
        // Avalia os individuos da População, seta geração e avalia os individuos
        $evaluatedPopulation = $this->evaluatePopulation($initialPopulation, $fitnessSolutionConfigs, 0);

          // $pop = sizeof($population);
          // dd($evaluatedPopulation);

        // Ordena os indivduos da população avaliada (individuo com melhor desempenho primeiro)
        $rankedPopulation = $this->array_msort($evaluatedPopulation, array('rating'=>SORT_ASC));
          // dd($rankedPopulation);

        // Adiciona a primeira população no vetor de populações (geração 1)
        $populations[] = $rankedPopulation;
      for ($p = 0; $p < $iterations; $p++) {



        // Gera nova população utilizando o crossover
        $newPopulation = $this->crossOver($populations[$p], $populationConfigs);
          // var_dump($newPopulation); echo '<br>';

        // Avalia os individuos da nova população
        $evaluatedNewPopulation = $this->evaluatePopulation($newPopulation, $fitnessSolutionConfigs, $p+1);
        // dd($evaluatedNewPopulation);

        // Ordena os individuos da nova população avaliada (individuo com melhor desempenho primeiro)
        $rankedNewPopulation = $this->array_msort($evaluatedNewPopulation, array('rating'=>SORT_ASC));

        // junta as duas populações e gera a nova população
        $generatedNewPopulation = $this->generatePoputalion($populations[$p], $rankedNewPopulation, $populationConfigs['populationsize']);

        // $nova = $this->array_msort(array_merge($rankedPopulation, $rankedNewPopulation), array('rating'=>SORT_ASC));
        $populations[] = $generatedNewPopulation;
      }
      return view('solutions.view', ['populations' => $populations]);

        dd($populations);



    	// array:12 [▼
  // "_token" => "ekDWxRoEPocj56iVX0JDbYlaUXN7FqWBRx35lHdb"
  // "problem_id" => "2"
  // "name" => "title solution"
  // "description" => "descrição da solução"
  // "population_size" => "10"
  // "individual_size" => "4"
  // "type" => "1"
  // "chromosome_size" => "1"
  // "chromosome_initial_value" => "0"
  // "chromosome_final_size" => "9"
  // "formula" => "($c1+$c2+$c3+$c4)/4"
  // "optimal_solution" => "5"
// ]

    }

    public function generatePoputalion($population, $newPopulation, $populationSize)
    {
      // $population = array();
      $nova = array_slice($this->array_msort(array_merge($newPopulation, $population), array('rating'=>SORT_ASC)), 0, $populationSize+2);
      return $nova;
        dd($nova);
    }

    public function evaluatePopulation($population, $fitnessSolutionConfigs, $generation)
    {
        foreach ($population as $key => $individual) {
          $individual['fitness'] = $fitnessSolutionConfigs['formula'].';';
          $individual['performance'] = $this->applyfitness($individual);
          $individual['generation'] = $generation;
          $individual['rating'] = abs($fitnessSolutionConfigs['optimal_solution'] - $individual['performance']);
          $evaluatedPopulation[$key] = $individual;
        }
        return $evaluatedPopulation;
    }

    public function crossOver($population, $options)
    {
      // dd($population);
      $populationsize = $options['populationsize'];
      $individualsize = $options['individualsize'];
      $chromosomesize = $options['chromosomesize'];
      $crossover = $options['crossover'];
      $newPopulation = array();
      // dd($populationsize);
      for ($i = 0; $i < ($populationsize/2); $i++) {
        if (count($newPopulation) >= $populationsize) {
          break;
        }
        if (($i+1)<($populationsize/2)) {

          for ($j = ($i+1); $j < ($populationsize/2); $j++) {
            if (count($newPopulation) >= $populationsize) {
              break;
            }
           // dd($population[0]['chromossomes']);
            for ($k = 0; $k < $individualsize; $k++) {
              if ($crossover[$k]) {
                $newIndividualChromossomes1[] = $population[$i]['chromossomes'][$k];
                $newIndividualChromossomes2[] = $population[$j]['chromossomes'][$k];
              }
              else {
                $newIndividualChromossomes1[] = $population[$j]['chromossomes'][$k];
                $newIndividualChromossomes2[] = $population[$i]['chromossomes'][$k];
              }
            }
            // echo 'gerou:';
            // print_r($newIndividualChromossomes1);
            // echo '<br>';
            // echo 'gerou:';
            // print_r($newIndividualChromossomes2);
            // echo '<br>';
            $newIndividual1['chromossomes'] = $newIndividualChromossomes1;
            $newIndividual2['chromossomes'] = $newIndividualChromossomes2;
            $newPopulation[] = $newIndividual1;
            $newPopulation[] = $newIndividual2;

                $newIndividualChromossomes1 = array();
                $newIndividualChromossomes2 = array();

          }
        }

      }
      return $newPopulation;
      dd($newPopulation);
      print_r($population);
    }

    public function newAG()
    {
      // $p = Population::find(13);
      // $i = $p->individuals;
      // // unset($i[3]);
      // $o = $i->random();
      // dd($i->count());
      // dd($i->where('id', 63)->keys()->first());
      $problems = Problem::get();
      return view('solutions.smartwizard', compact('problems'));
    }


}
