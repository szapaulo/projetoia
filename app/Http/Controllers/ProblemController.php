<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Problem;

class ProblemController extends Controller
{

    public function index(){
    	$problems = Problem::get();

    	return view('problems.index', ['problems' => $problems]);
    }

    public function create(Request $request){

        $problema = new Problem;
        $problema->user_id = 1;
        $problema->name = 'One Max';
        $problema->description = 'Maximizar quantidade de valores 1';
        $problema->save();

        // dd($problema);
        // $problem = $request->all();
        // $problem['user_id'] = 1;
        // Problem::create($problem);
        // return redirect()->route('problemas.index');
        // var_dump($problem);
        // $problem = Problem::
        // problem::create($request->all());


    }

    public function store(Request $request){

        $problema = new Problem();
        $problema->name = $request->name;
        $problema->description = $request->description;
        $problema->user_id = 1;
        // $problema->nao_existe = 'iajsiaf';
        // dd($problema);
        $problema->save();

        // print_r($request->all());
        // dd($problema);
        // $problem = $request->all();
        // $problem['user_id'] = 1;
        // Problem::create($problem);
        return redirect()->route('problemas.index');
        // var_dump($problem);
        // $problem = Problem::
        // problem::create($request->all());


    }

    public function show($id){

    	$problem = Problem::find($id);
    	// $solutions = Problem::find($id)->solutions;
    	// dd($solutions);
    	return view('problems.view', ['problem' => $problem]);

    }

    public function getDetails(Request $request)
    {
        // dd($request->problem_id);
        $problem = Problem::find($request->problem_id);
        $details_html = view('problems.partials.details', compact('problem'))->render();
        return response()->json([
            'html' => $details_html,
            'chromosome_quantity' => $problem->chromosome_quantity,
            'min_val' => $problem->min_val,
            'max_val' => $problem->max_val
        ]);
    }

    public function setProblem(Request $request)
    {
        $problem = Problem::find($request->problem_id);
        return response()->json($problem->toArray());
    }
}
