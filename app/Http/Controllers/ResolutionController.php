<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Solution;
use App\Models\Resolution;
use App\Models\Population;

class ResolutionController extends Controller
{

    //
    public function ajaxCreatePopulation(Request $request)
    {
    $solution = Solution::find($request->solution);
    $resolution = Resolution::firstOrCreate(["solution_id" => $solution->id]);
    $individual_size = $solution->individual_size;
    $initial_population = $resolution->initialPopulation();
    $population = $initial_population->create($solution);
    $response = array();
    $response['population_render'] = view('populations.show', compact('population'))->render();
    $response['chromossomes_select'] = view('chromosomes.options', compact('individual_size'))->render();
        return $response;
    if ($resolution->populations()->exists) {
        $resolution->generateNewPopulation();
    } else {
        $resolution->initialPopulation()->create($solution);
    }
    // $solution = Solution::create($request->form);
    return $resolution->id;
    }

    public function run(Request $request)
    {

        $res = Resolution::find(23);
        return view('resolutions.view', ['resolution' => $res]);

        // dd($initial_population);
        
        
    	// dd($request->all());
        $generation = 0;
    	$solution = Solution::find($request->solution_id);
    	// dd($solution->resolutions()->find(17)->populations()->first()->individuals()->first()->intChromosomes()->first());
    	$resolution = new Resolution();
    	$resolution->user_id = 1;
    	$resolution->solution_id = $solution->id;
    	$resolution->save();
        
        $initial_population = new Population(array('resolution_id' => $resolution->id, 'generation' => $generation));
    	// $initial_population = new Population();
    	// $initial_population->resolution_id = $resolution->id;
    	// $initial_population->generation = $generation;
    	$initial_population->save();
    	$initial_population->create($solution);

    	// dd('reso');
    	// dd($solution);

    	$iterations = 10;
    	// $initial_population = Population::find(1);
    	// $resolution = $initial_population->resolution;

        // return view('resolutions.view', ['resolution' => $resolution]);
        // $solution = $resolution->solution;
        $individual_size = $solution->individual_size;
        $crossover = $solution->crossover;
        // dd($solution);
        //     "id" => 4
        //     "user_id" => 1
        //     "formula_id" => 6
        //     "mutation_id" => 1
        //     "population_size" => 7
        //     "individual_size" => 5
        //     "name" => "Solução 1"
        //     "description" => "descrição da solução"
        //     "optimal_solution" => "90"
        //     "chromosome_initial_value" => 1
        //     "chromosome_final_size" => 100
        //     "chromosome_size" => null
        //     "crossover" => "10010"
        //     "type" => "1"
        //     "created_at" => "2018-04-21 17:25:18"
        //     "updated_at" => "2018-04-21 17:25:18"
        //     "problem_id" => 1

        $initial_population->evaluate();


        // dd($initial_population->individuals()->orderBy('fitness_solution')->get());
        // $individuos = $initial_population->individuals()->orderBy('fitness_solution')->get();
        // dd($individuos);
        // dd($individuos);

    	// dd($initial_population);
       

		for ($i = 0; $i < $iterations; $i++) {
            $generation++;
            $newPopulaton = new Population(array('resolution_id' => $resolution->id, 'generation' => $generation));
            // $newPopulaton->resolution_id = $resolution->id;
            // $newPopulaton->generation = $generation;
            $newPopulaton->save();
            $newPopulaton->crossover();
            if ($newPopulaton->evaluate())
                break;
            
		}


        return view('resolutions.view', ['resolution' => $resolution]);





        dd('aqui');
    	dd($resolution->id);
    }
}
