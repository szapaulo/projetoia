<?php

namespace App\Http\Controllers\Api;

use App\Models\ToolConsumer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LtiController extends Controller
{
    public function connect(Request $request)
    {
        return view('index');
        $consumer_key = $request->oauth_consumer_key;
        $tool_consumer = ToolConsumer::where('consumer_key', $consumer_key)->first();
    	// dd($request);

        if (!$tool_consumer) {
            return false;
        }

        $tool_consumer->course_id = $request->context_id;
        $tool_consumer->course_name = $request->context_title;
        $tool_consumer->course_label = $request->context_label;
        $tool_consumer->save();

        $user = User::firstOrNew(['original_id' => $request->user_id]);
        $user->name = $request->lis_person_name_full;
        $user->email = $request->lis_person_contact_email_primary;
        $user->role = $request->roles;
        $user->original_id = $request->user_id;
        $user->save();

        $tool_consumer->users->contains($user)? NULL: $tool_consumer->users()->attach($user, ['role' => $request->roles]);
        Auth::login($user);
        return view('index');

        dd(Auth::user());

    }
}
