<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Problem extends Model
{
    protected $fillable = [
        'name', 'description', 'user_id'
    ];

    public function solutions()
    {
    	return $this->hasMany('App\Models\Solution');
    }
}
