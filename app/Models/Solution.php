<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solution extends Model
{

	protected $fillable = [
        'name', 'description', 'user_id', 'formula_id', 'mutation_id', 'population_size', 'individual_size', 'optimal_solution', 'chromosome_initial_value', 'chromosome_final_size', 'chromosome_size', 'type', 'problem_id', 'chromosome_size', 'crossover'
    ];
    

    public function formula(){
    	return $this->belongsto('App\Models\Formula');
    }

    public function problem(){
    	return $this->belongsto('App\Models\Problem');
    }

      /**
     * Get the comments for the blog post.
     */
    public function resolutions()
    {
        return $this->hasMany('App\Models\Resolution');
    }
}
