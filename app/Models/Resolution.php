<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class resolution extends Model
{

	protected $fillable = [
        'user_id', 'solution_id'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function solution()
    {
        return $this->belongsTo('App\Models\Solution');
    }

    /**
     * Get the comments for the blog post.
     */
    public function populations()
    {
        return $this->hasMany('App\Models\Population');
    }

    public function initialPopulation()
    {
        return $this->populations()->create(array('generation' => 0));
    }

    public function generateNewPopulation()
    {
        # code...
    }
}
