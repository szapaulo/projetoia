<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IntChromosome extends Model
{
    protected $fillable = [
        'individual_id', 'parent_id', 'value'
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

    }
	public function individual()
	{
		return $this->belongsto('App\Models\Individual');
	}

	public function parent()
	{
		return $this->belongsto('App\Models\intChromosome', 'parent_id');
	}

    public function childs()
    {
    	return $this->hasMany('App\Models\intChromosome', 'parent_id');
    }

    public function create(Solution $solution)
    {
    	$chromosomesize = $solution->chromosome_size;
    	$minval = $solution->chromosome_initial_value;
    	$maxval = $solution->chromosome_final_size;
    	$this->value = rand($minval,$maxval);
    	$this->save();
    	// dd($this);
    }
}
