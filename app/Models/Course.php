<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    CONST INSTRUCTOR = 1;
    CONST LEARNER = 2;

	public function toolConsumer()
	{
		return $this->belongsTo('App\Models\ToolConsumer');
	}

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('role');
    }

    public function instructor()
    {
    	return $this->users()->wherePivot('role', self::INSTRUCTOR);
    }

    public function students()
    {
    	return $this->users()->wherePivot('role', 2);
    }
}
