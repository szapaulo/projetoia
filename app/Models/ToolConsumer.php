<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ToolConsumer extends Model
{
    public function course()
    {
    	return $this->hasOne('App\Models\Course');
    }

    public function users()
    {
    	return $this->belongsToMany('App\User')->withPivot('role');
    }
}
