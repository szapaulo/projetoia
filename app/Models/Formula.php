<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Formula extends Model
{

	protected $fillable = [
        'user_id', 'formula', 'type', 'name', 'description'
    ];

    public function solutions()
    {
    	return $this->hasMany('App\Models\Solution');
    }
}
