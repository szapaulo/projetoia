<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\IntChromosome;

class Individual extends Model
{
    protected $fillable = [
        'created_at_generetion', 'parent1_id', 'parent2_id', 'fitness_solution'
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

    }

    public function intChromosomes()
    {
    	return $this->hasMany('App\Models\IntChromosome');
    }

	public function parent1()
	{
		return $this->belongsto('App\Models\Individual', 'parent1_id');
	}

    public function childs1()
    {
    	return $this->hasMany('App\Models\Individual', 'parent1_id');
    }

	public function parent2()
	{
		return $this->belongsto('App\Models\Individual', 'parent2_id');
	}

    public function childs2()
    {
    	return $this->hasMany('App\Models\Individual', 'parent2_id');
    }


    /**
     * The roles that belong to the user.
     */
    public function Populations()
    {
        return $this->belongsToMany('App\Models\Population')->withTimestamps()->withPivot('index', 'rank', 'performance');
    }



    public function crossover($chromosomesP1, $chromosomesP2)
    {
        $crossover = $this->Populations->first()->resolution->solution->crossover;
        $individualsize = $this->Populations->first()->resolution->solution->individual_size;
        for ($i = 0; $i < $individualsize; $i++) {
            // $chromosome = new IntChromosome();
            // $chromosome->individual_id = $this->id;
            if ($crossover[$i]) {
                $chromosomeP = $chromosomesP1[$i];
                // $chromosome->value = $chromosomesP1[$i]->value;
                // $chromosome->parent_id = $chromosomesP1[$i]->id;
            } else {
                $chromosomeP = $chromosomesP2[$i];
                // $chromosome->value = $chromosomesP2[$i]->value;
                // $chromosome->parent_id = $chromosomesP2[$i]->id;
            }
            $chromosome = new IntChromosome(array('individual_id' => $this->id, "value" => $chromosomeP->value, 'parent_id' => $chromosomeP->id));
            $chromosome->save();
        // dd($chromosome);
        }

    }


    public function create(Solution $solution)
    {
        $individualsize = $solution->individual_size;
        for ($i = 0; $i < $individualsize; $i++) {
            $chromosome = new IntChromosome(array('individual_id' => $this->id));
            // $chromosome->individual_id = $this->id;
            $chromosome->create($solution);
        // dd($chromosome);
        }
    }
}
