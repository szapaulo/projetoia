<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Solution;
use App\Models\Individual;

class Population extends Model
{
    protected $fillable = [
        'resolution_id', 'generation'
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

    }

    /**
     * The roles that belong to the user.
     */
    public function individuals()
    {
        return $this->belongsToMany('App\Models\Individual')->withPivot('index', 'rank', 'performance');
    }

    /**
     * Get the resolutions that owns the population.
     */
    public function resolution()
    {
        return $this->belongsTo('App\Models\Resolution');
    }

    function cross_individuals($parent1, $parent2)
    {
    	$individual = new Individual();
        $individual->created_at_generetion = $this->generation;
        $individual->parent1_id = $parent1->id;
        $individual->parent2_id = $parent2->id;

        $individual->save();
        $this->individuals()->attach($individual);
        $individual->crossover($parent1->intChromosomes, $parent2->intChromosomes);
}

    public function crossover()
    {
        $populationsize = $this->resolution->solution->population_size;
        $prev_population = $this->resolution->populations()->orderBy('id', 'desc')->skip(1)->take(1)->first();
        $prev_individuals = $prev_population->individuals()->orderBy('fitness_solution')->get();

        for ($i=0; $i < $populationsize-1; $i++) {
            if ($this->individuals()->count() == $populationsize)
                break;

            $parent1 = $prev_individuals[$i];
            $parent2 = $prev_individuals[$i+1];
            $this->cross_individuals($parent1, $parent2);
            if ($this->individuals()->count() == $populationsize)
                break;

            $this->cross_individuals($parent2, $parent1);


        }

        // dd($this->individuals);
    }

    public function create(Solution $solution)
    {
    	// dd('solution');

		$populationsize = $solution->population_size;

		for ($i=0; $i < $populationsize; $i++) {
			$individual = new Individual(array('created_at_generetion' => $this->generation));
			// $individual->created_at_generetion = $this->generation;
			$individual->save();
			$individual->create($solution);
    	// dd($individual);
			$this->individuals()->attach($individual);
		}
        return $this;






		// $minval = $options['minval'];
		// $maxval = $options['maxval'];

		// for ($i=0; $i < $populationsize; $i++) {
		// for ($j = 0; $j < $individualsize; $j++) {
		//   $number = rand($minval,$maxval);
		//   $chromossome[] = sprintf('%0'.$chromosomesize.'d',$number);
		// }
		// $individual['chromossomes'] = $chromossome;
		// $population[] = $individual;
		// $chromossome = array();
  //   }
    }

    public function evaluate()
    {
        $flag= FALSE;
        // dd($this->individuals()->count());
        $individuals = $this->individuals;
        $formula = $this->resolution->solution->formula.';';
        // $optimal_solution = $this->resolution->solution->optimal_solution;
        // dd($individuals->count());
        // dd($formula);
        // dd($optimal_solution);

        // dd($this);
        foreach ($individuals as $individual) {
            $chromosomes = $individual->intChromosomes;
            $c = array();
            // dd($chromosomes->count());
            foreach ($chromosomes as $chromosome) {
                $c[] = $chromosome->value;
            }
            // dd($c);
            // dd($formula);
            eval($formula);
            $individual->fitness_solution = $solution;
            // $individual->fitness_solution = abs($optimal_solution - $solution);
            $individual->save();
            if($individual->fitness_solution == 0)
                $flag = TRUE;
    //      dd($individual);
    //      dd($solution);
    //      dd($chromossomes);
          // $individual['fitness'] = $fitnessSolutionConfigs['formula'].';';
    //       $individual['performance'] = $this->applyfitness($individual);
    //       $individual['generation'] = $generation;
    //       $individual['rating'] = abs($fitnessSolutionConfigs['optimal_solution'] - $individual['performance']);
    //       $evaluatedPopulation[$key] = $individual;

    //       $fitness = $individual['fitness'];
       //    $chromossomes = $individual['chromossomes'];
       //    eval($fitness);
       //    return $solution;
        }
        // echo 'string';
        // exit;
        return $flag;
    }


// 1 - CRESCENTE
// 2 - DECRESCENTE
// 3 - APROXIMAÇÃO
    public function avaliate($avaliation_method)
    {
        $individuals = $this->individuals;
        $optimal_solution = $this->resolution->solution->optimal_solution;
        $sum_fitness = $this->individuals->sum('fitness_solution');

        $rank = 1;
        switch ($avaliation_method) {
            case 1:

                foreach ($this->individuals->sortBy('fitness_solution') as $individual) {
                    $this->individuals()->updateExistingPivot($individual->id, ['rank' => $rank]);
                    $rank++;
                }
                break;
            case 2:

                foreach ($this->individuals->sortByDesc('fitness_solution') as $individual) {
                    $this->individuals()->updateExistingPivot($individual->id, ['rank' => $rank]);
                    $rank++;
                }
                break;
            case 3:

                foreach ($this->individuals as $individual) {
                    $individual->performance = abs($individual->fitness_solution - $optimal_solution);
                    $individual->save();
                }
                foreach ($this->individuals->sortByDesc('performance') as $individual) {
                    $individual->pivot->rank = $rank;
                    $individual->save();
                    $rank++;
                }
                break;
        }
    }
}
