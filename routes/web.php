<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('problemas','ProblemController');
Route::resource('solucoes','SolutionController');
Route::resource('resolucao','ResolutionController');
Route::post('solucao/rodar', 'ResolutionController@run')->name('resolucao.rodar');
Route::post('solucao/criar', 'SolutionController@create')->name('solucao.criar');
Route::post('solucao/calcular', 'SolutionController@calculate')->name('solucao.calcular');

Route::post('problema/salvar', 'ProblemController@add');
Route::get('problema/view/{id}', 'ProblemController@add');
Route::get('criarpop', 'SolutionController@createPopulation');
// Route::get('teste', 'SolutionController@teste');
// Route::get('problemas', 'ProblemController@index')->name('problemas.listar');

Route::get('/', function () {
    return view('index');
});

Route::get('test', function () {
    return view('solutions.wizard');
});


Route::get('new_ag', 'SolutionController@newAG')->name('new_ag');
Route::post('ajax/problem/details', 'ProblemController@getDetails');
Route::post('ajax/problem/set', 'ProblemController@setProblem');

Route::get('problema/criar', function () {
    return view('problems.create');
})->name('problema.criar');

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Route::post('solucao/ajaxcreate', 'SolutionController@ajaxCreate');
Route::post('solucao/ajaxavaliate', 'SolutionController@ajaxAvaliate');
Route::post('solucao/ajax/saveformula', 'SolutionController@ajaxSaveFormula');
Route::post('solucao/ajaxinitialpopulation', 'SolutionController@ajaxInitialPopulation');
Route::post('solucao/ajaxcreatepopulation', 'ResolutionController@ajaxCreatePopulation');
Route::post('solucao/ajaxcrossover', 'SolutionController@ajaxCrossover');
Route::post('solucao/ajaxgeneratenewpopulation', 'SolutionController@ajaxGenerateNewPopulation');
Route::post('solucao/ajaxfinish', 'SolutionController@ajaxFinish');

Route::get('menu', function() {
    return view('home.main_menu');
})->name('main.menu') ;

Route::get('teoria', function() {
    return view('home.teoria');
})->name('teoria') ;