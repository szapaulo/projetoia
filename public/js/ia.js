


$(document).ready(function(){
  // $(":radio").labelauty();
  // $(":checkbox").labelauty();

$('#smartwizard').smartWizard({
   theme: 'arrows',
   useURLhash: false,
   showStepURLhash: false,
   autoAdjustHeight: false,
   lang: {  // Language variables
    next: 'Próximo',
    previous: 'Anterior'
  },
});

$('.sw-btn-next').attr('disabled', true);
// alert('oi');



   $('#setindividualsize').on('click', function () {
   		$('#solution_chromosomes').empty();
   		$('#individualA').empty();
   		$('#individualB').empty();
   		$('#solution_chromosomes').empty();
   		$('#formula').val('$solution = ');
   		$('#formula-display').val('');
   		var checkedA = 'checked';
   		var checkedB = '';
   		var activeA = 'active';
   		var activeB = '';
   		var individual_size = $('#individual_size').val();
   		for (var i = 0; i < individual_size; i++) {
   			if((i+1)>(individual_size/2)){ checkedA = ''; checkedB = 'checked'; activeA = ''; activeB = 'active';}
   			$('#solution_chromosomes').append('<button type="button" class="btn btn-dark variables" title="cromossomo'+(i+1)+'" data="$chromossomes['+(i)+']">C'+(i+1)+'</button>');
   			$('#individualA').append('<div class="chromossome '+activeA+'"><label class="custom-control custom-radio"><input name="chromo'+i+'" type="radio" value="1" class="custom-control-input" '+checkedA+'><span class="custom-control-indicator"></span></label></div>');
   			$('#individualB').append('<div class="chromossome '+activeB+'"><label class="custom-control custom-radio"><input name="chromo'+i+'" type="radio" value="0" class="custom-control-input" '+checkedB+'><span class="custom-control-indicator"></span></label></div>');
   		}
   		// alert($('#individual_size').val());
   });

  // $('.operator').on('click', function() {
  //   var input = $('#formula');
  //   var input_display = $('#formula-display');
  //   var operator = $(this).attr('data');
  //   input.val(input.val()+operator);
  //   input_display.val(input_display.val()+operator);
  //   /* Act on the event */
  // });

  $('.formula_operator').on('change', function() {
    var input = $('#formula');
    var input_display = $('#formula-display');

    var operator = this.value;
    var operator_display = this.options[this.selectedIndex].innerHTML;

    input.val(input.val()+operator);
    input_display.val(input_display.val()+operator_display);
    $(this).prop('selectedIndex',0);
    /* Act on the event */
  });


   // $('#create_solution').on('click', function() {
   //  var csrf = $("input[name=_token]").val();
   //  var solution_id ={
   //    id: $("#solution_id").val()
   //  }
   //  var formData = {
   //    problem_id: $('#problem_id').val(),
   //    population_size: $('#population_size').val(),
   //    individual_size: $('#individual_size').val(),
   //    chromosome_initial_value: $('#chromosome_initial_value').val(),
   //    chromosome_final_size: $('#chromosome_final_size').val()
   //  }
   //  $.ajax({
   //      type:'POST',
   //      url:'solucao/ajaxcreate',
   //      data: { form: formData, _token: csrf,  solution: solution_id},
   //      success:function(solution_id){
   //        $("#solution_id").val(solution_id);swal({
   //          type: 'success',
   //          title: 'Configurações salvas!',
   //          showConfirmButton: false,
   //          timer: 1500
   //        })
   //        // $("#msg").html(data.msg);
   //      }
   //  });

   // });


$('#create_initial_populationa').on('click', function() {
  var csrf = $("input[name=_token]").val();
  var solution_id ={
                  id: $("#solution_id").val()
  }
  var formData = {
                  problem_id: $('#problem_id').val()
                  }
        console.log(formData);
  $.ajax({
     type:'POST',
     url:'solucao/ajaxcreate',
     data: { form: formData, _token: csrf,  solution: solution_id},
     success:function(solution_id){
        $("#solution_id").val(solution_id);


        $.ajax({
           type:'POST',
           url:'solucao/ajaxcreatepopulation',
           data: { solution: solution_id, _token: csrf},
           success:function(data){
            alert('ops');
            console.log(data.population_render);
            $('#initial_population').html(data.population_render);
              // $("#solution_id").val(data);
              // $("#msg").html(data.msg);
           }
        });

        // $("#msg").html(data.msg);
     }
  });
});
      $('.variables').on('click', function() {
        var input = $('#formula');
        var input_display = $('#formula-display');

        var variable = $(this).attr('data');
        var display = $(this).html();

        input.val(input.val()+variable);
        input_display.val(input_display.val()+display);
        /* Act on the event */
      });

$('#reset_formula').click(function(event) {
  $('#formula').val('$solution=');
  $('#formula-display').val('');
});

});

function setBind() {
   $('.individual').on('mouseover', function () {
      parent1_id = jQuery(this).attr('data-parent1');
      parent2_id = jQuery(this).attr('data-parent2');
      origin = jQuery(this).attr('data-iid');
      // jQuery( "[data-id='"+parent1_id+"']" ).css('color', 'red');
      // jQuery( "[data-id='"+parent2_id+"']" ).css('color', 'red');
      jQuery( "[data-iid='"+parent1_id+"']" ).addClass('active');
      jQuery( "[data-iid='"+parent2_id+"']" ).addClass('active');
      jQuery( "[data-iid='"+origin+"']" ).addClass('track');
   });

   $('.individual').on('mouseout', function () {
      parent1_id = jQuery(this).attr('data-parent1');
      parent2_id = jQuery(this).attr('data-parent2');
      origin = jQuery(this).attr('data-iid');
      // jQuery( "[data-id='"+parent1_id+"']" ).css('color', '#007bff');
      // jQuery( "[data-id='"+parent2_id+"']" ).css('color', '#007bff');
      jQuery( "[data-iid='"+parent1_id+"']" ).removeClass('active');
      jQuery( "[data-iid='"+parent2_id+"']" ).removeClass('active');
      jQuery( "[data-iid='"+origin+"']" ).removeClass('track');
   });


   $('.gene').on('mouseover', function () {
      parent_id = jQuery(this).attr('data-parent');
      // alert("[data-id='"+parent_id+"']")
      // jQuery( "[data-id='"+parent_id+"']" ).css('color', 'yellow');
      jQuery( "[data-gid='"+parent_id+"']" ).addClass('active');
   });

   $('.gene').on('mouseout', function () {
      parent_id = jQuery(this).attr('data-parent');
      // jQuery( "[data-id='"+parent_id+"']" ).css('color', 'black');
      jQuery( "[data-gid='"+parent_id+"']" ).removeClass('active');
   });
   }

function increase_individual(chromosome_number){
  clear_chromossomes();
  var chromosome = chromosome_number;
  var key = (parseInt(chromosome) - 1);
  var individual = 1;
  $("#mytable tr").each(function() {
    $(this).append('<td class="i-'+ individual +' c-'+ chromosome +'"><input class="chromosome_value" id="c-'+ individual +'-'+ chromosome +'" type="number" name="population['+ individual +']['+ chromosome +']"></td>');
    individual++;
  });
  $('#variables').append('<option value="$c['+ key +']" class="variables">C'+chromosome+'</option>');


};

function decrease_individual(){
  clear_chromossomes();
  $("#mytable tr").each(function() {
    $(this).find('td:last').remove();
  });
  $('#variables').find('option:last').remove();
};

function increase_population(){
  clear_chromossomes();
  var pop_size = $('#population_size').val();
  var cols_qty = $("#mytable tr:first td").length;
  $("#mytable").append(
    "<tr>" +
      (function() {
        var cols = "";
        for (var i = 0; i < cols_qty; i++) {
          cols += '<td class="i-'+ pop_size +' c-'+ (i+1) +'"><input class="chromosome_value" id="c-'+ pop_size +'-'+ (i+1) +'" type="number" name="population['+ pop_size +']['+ (i+1) +']"></td>';
        }
        cols += "</tr>";
        return cols;
      })()
  );
};
function clear_chromossomes(){
    $('.chromosome_value').val('');
}

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}
$('#chromosome_initial_value').change(function(event) {
  $('.chromosome_value').attr('min', $(this).val());
    clear_chromossomes();
});
$('#chromosome_final_size').change(function(event) {
  $('.chromosome_value').attr('max', $(this).val());
    clear_chromossomes();
});

$('#generate_population').click(function(event) {
    $('.chromosome_value').each(function(index, el) {
        $(this).val(getRndInteger(0, 1));
    });
});

$('#generate_crossover').click(function(event) {

        for (var i = 0; i < $('#individual_size').val(); i++) {
          var rand = getRndInteger(0, 1);
          if (rand) {
            $('#Achromo'+i).prop("checked", true);
            $('#Bchromo'+i).prop("checked", false);
          } else {
            $('#Bchromo'+i).prop("checked", true);
            $('#Achromo'+i).prop("checked", false);
          }
        }
});

function add_chromosomea(){
  ind_size = $('#individual_size').val();
  $('#individualA').append('<div class="chromossome custom-control custom-radio custom-control-inline"><input name="chromo'+ ind_size +'" id="Achromo'+ ind_size +'" type="radio" value="1" class="custom-control-input"><label class="custom-control-label" for="Achromo'+ ind_size +'"></label></div>');
  $('#individualB').append('<div class="chromossome custom-control custom-radio custom-control-inline"><input name="chromo'+ ind_size +'" id="Bchromo'+ ind_size +'" type="radio" value="0" class="custom-control-input"><label class="custom-control-label" for="Bchromo'+ ind_size +'"></label></div>');
};

function remove_chromosome(){
  $('#individualB').find('div:last').remove();
  $('#individualA').find('div:last').remove();
};

$("#inc_pop").click(function() {
  $('#population_size').get(0).value++;
  increase_population();
});

$("#dec_pop").click(function() {
  clear_chromossomes();
  var pop_size = parseFloat($('#population_size').val());
  if(pop_size > 2){
    $('#population_size').get(0).value--;
    $("#mytable tr:last").remove();
  }
});

$("#inc_ind").click(function() {
  $('#individual_size').get(0).value++;
  var chromosome_number = $('#individual_size').val();
  increase_individual(chromosome_number);
  add_chromosome();
});

$("#dec_ind").click(function() {
  var ind_size = parseFloat($('#individual_size').val());
  if(ind_size > 2){
    $('#individual_size').get(0).value--;
    decrease_individual();
    // remove_chromosome();
  }
});

$('.close-helper').click(function(){
$('.full-helper').slideUp('slow');
});

$('.help-icon').click(function(){
$(this).closest('.help-message').find('.full-helper').slideDown();
});

var problem;
var chromosome_quantity;
var min_val;
var max_val;

$('#problem_select').change(function(){
  // alert($(this).val());
  var problem = $(this).val();
  jQuery.ajax({
      type: "POST",
      url: "ajax/problem/details",
      data: {problem_id: problem},
      success: function( data )
      {
        $('#problem_details').html(data.html);
      },
      'beforeSend': function (request) {
          request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
          $('.overlay').fadeIn('fast');
      },
      'complete': function(){$(".overlay").fadeOut('fast');}
  });
});

$('#problem_form').submit(function(e){
  e.preventDefault();
  if(!$('#problem_select').val()) {
    swal({
      type: 'error',
      title: 'Selecione um Problema'
    })
    return;
  }

  swal({
    title: 'Confirmar?',
    text: "Para alterar o problema terá que iniciar outro AG",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Confirmo!'
  }).then((result) => {
    if (result.value) {
        jQuery.ajax({
        type: "POST",
        url: "ajax/problem/set",
        data: jQuery( this ).serializeArray(),
        success: function( data )
        {
          console.log(data);
          problem = data;

          if (problem.chromosome_quantity) {
            $('#individual_size').val(problem.chromosome_quantity);
            for (var i = 3; i <= problem.chromosome_quantity; i++) {
              increase_individual(i);
              $('#dec_ind').attr('disabled', true);
              $('#inc_ind').attr('disabled', true);
            }
          }

          if (problem.min_val != null) {
            $('#chromosome_initial_value').val(problem.min_val);
            $('#chromosome_initial_value').attr('readonly', true);
          }

          if (problem.max_val != null) {
            $('#chromosome_final_size').val(problem.max_val);
            $('#chromosome_final_size').attr('readonly', true);
          }

          $('#problem_select').attr('disabled', true);
          $('#set_problem').attr('disabled', true);


          $('.sw-btn-next').attr('disabled', false);
        },
        'beforeSend': function (request) {
            request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            $('.overlay').fadeIn();
        },
        'complete': function(){$(".overlay").fadeOut();}
        });


    }
  })

});

jQuery('#create_solution_form').submit(function(e){
  e.preventDefault();
  var dados = jQuery( this ).serializeArray();
  dados.push({name: 'problem_id', value: $('#problem_id').val()});
  dados.push({name: 'solution_id', value: $('#solution_id').val()});

  jQuery.ajax({
      type: "POST",
      url: "solucao/ajaxcreate",
      data: dados,
      success: function( data )
      {
        problem.solution = data;
        $('#solution_id').val(problem.solution.id);
        swal({
          type: 'success',
          title: 'Configurações Salvas!',
          showConfirmButton: false,
          timer: 1500
        })
        $('#generate_population').attr('disabled', false);
        $('#submit_initial_population').attr('disabled', false);

        for (var i = 0; i < $('#individual_size').val(); i++) {
          $('#individualA > .row').append('<div class="col-md-12"><div class="chromossome form-check"><input name="chromo['+i+']" id="Achromo'+i+'" type="radio" value="1" class="form-check-input gene-selection" data-labelauty="'+(i+1)+'"><label class="cusm-control-label" for="Achromo'+i+'"></label></div></div>');
          $('#individualB > .row').append('<div class="col-md-12"><div class="chromossome form-check"><input name="chromo['+i+']" id="Bchromo'+i+'" type="radio" value="0" class="form-check-input gene-selection" data-labelauty="'+(i+1)+'"><label class="cusm-control-label" for="Bchromo'+i+'"></label></div></div>');
        }
              $(".gene-selection").labelauty({ icon: false });
              $('#generate_crossover').trigger('click')
      },
      'beforeSend': function (request) {
          request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
          $('.overlay').fadeIn();
      },
      'complete': function(){$(".overlay").fadeOut();}
  });
});

jQuery('#submit_initial_population').click(function(e){
        var dados = jQuery('#create_initial_population').serializeArray();
        dados.push({name: 'solution_id', value: $('#solution_id').val()});

        jQuery.ajax({
            type: "POST",
            url: "solucao/ajaxinitialpopulation",
            data: dados,
            success: function( data )
            {
              console.log(data);
              swal(
                'População Inicial Gerada com Sucesso!',
                '',
                'success'
              )
              $('#create_solution').attr('disabled', true);
              $('#generate_population').attr('disabled', true);
              $('#submit_initial_population').attr('disabled', true);
              problem.solution.populations = new Array();
              problem.solution.populations.push(data);
            },
            'beforeSend': function (request) {
                request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                $('.overlay').fadeIn();
            },
            'complete': function(){$(".overlay").fadeOut();}
        });
});

jQuery('#formula_form').submit(function(e){
        e.preventDefault();
        var dados = jQuery( this ).serializeArray();
        dados.push({name: 'solution_id', value: $('#solution_id').val()});

        jQuery.ajax({
            type: "POST",
            url: "solucao/ajax/saveformula",
            data: dados,
            success: function( data )
            {
              $('#avaliated_population').html(data);
            },
            'beforeSend': function (request) {
                request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                $('.overlay').fadeIn();
            },
            complete: function(){$(".overlay").fadeOut();}
        });
});

jQuery('#avaliation_form').submit(function(e){
        e.preventDefault();
        var dados = jQuery( this ).serializeArray();
        dados.push({name: 'solution_id', value: $('#solution_id').val()});

        jQuery.ajax({
            type: "POST",
            url: "solucao/ajaxavaliate",
            data: dados,
            success: function( data )
            {
              $('#avaliated_population').html(data.selected_individuals);
              $('#selected_individuals').html(data.selected_individuals);
              // console.log(data);
            },
            'beforeSend': function (request) {
                request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                $('.overlay').fadeIn();
            },
            complete: function(){$(".overlay").fadeOut();}
        });
});

jQuery('#generate_new_population_form').submit(function(e){
        e.preventDefault();
        var dados = jQuery( this ).serializeArray();
        dados.push({name: 'solution_id', value: $('#solution_id').val()});
        jQuery.ajax({
            type: "POST",
            url: "solucao/ajaxgeneratenewpopulation",
            data: dados,
            success: function( data )
            {
              console.log(data);
              $('#new_individuals').html(data.view);
              setBind();
              // $('#avaliated_population').html(data.view);
              // $('#selected_individuals').html(data.selected_individuals);
              // console.log(data);
            },
            'beforeSend': function (request) {
                request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                $('.overlay').fadeIn();
            },
            complete: function(){$(".overlay").fadeOut();}
        });
});

jQuery('#finish_form').submit(function(e){
        e.preventDefault();
        var dados = jQuery( this ).serializeArray();
        dados.push({name: 'solution_id', value: $('#solution_id').val()});
        jQuery.ajax({
            type: "POST",
            url: "solucao/ajaxfinish",
            data: dados,
            success: function( data )
            {
              console.log(data);
              $('#final_result').html(data.view);
              setBind();
              // setBind();
              // $('#avaliated_population').html(data.view);
              // $('#selected_individuals').html(data.selected_individuals);
              // console.log(data);
            },
            'beforeSend': function (request) {
                request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                $('.overlay').fadeIn();
            },
            complete: function(){$(".overlay").fadeOut();}
        });
});

jQuery('#crossover_form').submit(function(e){
        e.preventDefault();
        var dados = jQuery( this ).serializeArray();
        dados.push({name: 'solution_id', value: $('#solution_id').val()});
        jQuery.ajax({
            type: "POST",
            url: "solucao/ajaxcrossover",
            data: dados,
            success: function( data )
            {
              console.log(data);
              $('#new_individuals').html(data.view);
              setBind();
              // $('#avaliated_population').html(data.view);
              // $('#selected_individuals').html(data.selected_individuals);
              // console.log(data);
            },
            'beforeSend': function (request) {
                request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                $('.overlay').fadeIn();
            },
            complete: function(){$(".overlay").fadeOut();}
        });
});

function funcao(argument) {
  console.log(argument.value);
  $(argument).prop('selectedIndex',0);

}

$('#avaliation_method').change(function(event) {
  if (this.value == 3) {
    $('#optimal_solution').fadeIn('fast');
  } else {
    $('#optimal_solution').fadeOut('fast');
  }
});